---

theme: gaia
_class: lead
paginate: true
backgrfalseeeColor: #fff
marp: true
backgroundImage: url('https://marp.app/assets/hero-background.jpg')

---

![bg left:47% 95%](https://www.ittelo.ru/upload/iblock/d5f/d5fe59e9975bfcf99b8386e42fd6a70d.jpg)

# **Devops**

**Попов Антон**

https://gitlab.com/antonholmes

---

**DevOps** (DEVelopment OPeration) – это набор практик для повышения эффективности процессов разработки (Development) и эксплуатации (Operation) программного обеспечения (ПО) за счет их непрерывной интеграции и активного взаимодействия профильных специалистов с помощью инструментов автоматизации.

![bg left:50% 100%](https://acn-marketing-blog.accenture.com/wp-content/uploads/2017/12/Accenture-Breaking-Development-Operations_1.jpg)

---

![bg left:100% 100%](https://simpleone.ru/wp-content/uploads/2020/08/devops_02@2x.png)

---

![bg left:100% 100%](https://i0.wp.com/digitalvarys.com/wp-content/uploads/2019/04/DevOps-Tools.png?fit=2109%2C1216&ssl=1)

---

![bg left:100% 100%](https://jet.su/time2market/images/tild3035-3239-4233-b863-353233663766__big_repaired.svg)

---


![bg left:100% 100%](https://media.slid.es/uploads/147321/images/6211365/pasted-from-clipboard.png)

---

### Инфраструктура как код 
###### Infrastructure as code (IAC)

1. Экономим время
2. Знаем какой получится результат
3. Независимось от железа

![bg right:43% 100%](https://docs.microsoft.com/en-us/devops/_img/infrastructureascode_600x300-3.png)


---
![bg right:50% 100%](https://cdn.mos.cms.futurecdn.net/EFgXKNVuN9gZ2wZThqdeYC.jpg)

![bg left:100% 100%](https://www.veritis.com/wp-content/uploads/2019/03/cloud-vs-on-premise-it-infrastructure-model-of-your-choice.jpg)

---

### Инфраструктура как код

* Придерживаться лучших практик
* Максимально понятный код
* Переиспользование
* Выносим переменные 


---

##### Основные возможности

* полноценное версионирование
* тестирование инфраструктуры 
* полный цикл тестирования кода dev > test > stage > prog
* наследование паттернов программирования 
* гарантированный результат
* непрерывная интеграция
* непрерывная доставка
