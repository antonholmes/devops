# Install Docker и (первый запуск контейнера hello world)

## Установка Docker

### Требования к OS:
Для установки Docker вам нужна 64 битная версия Debian или любая другая OS Linux:
- Docker поддерживается на x86_64 (или amd64), armhf и arm64 архитектурах.

Для того чтобы получить наиболее актуальную версию программы, необходимо обратиться к официальному репозиторию Docker.

Перед установкой установкой Docker на новом хосте, в первую очередь необходимо подключить Docker репозиторий. После чего произвести установку или обновление Docker.

##### Подключение репозитория
Обновим индекс пакетов:

```bash
sudo apt update
```

Установим зависимости:
```bash
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

Загрузим и установим пакет Docker. Добавим в систему GPG-ключ репозитория Docker:

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
Добавим этот репозиторий Docker в APT:

```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
```

Обновим индекс пакетов:
```bash
    sudo apt update
```
Следующая команда позволит переключиться из репозитория Debian в репозиторий Docker:
```bash
    apt-cache policy docker-ce
```
Команда вернет следующую информацию:

    docker-ce:
    Installed: (none)
    Candidate: 5:18.09.7~3-0~debian-buster
    Version table:
    5:18.09.7~3-0~debian-buster 500
    500 https://download.docker.com/linux/debian buster/stable amd64 Packages

Установим DockerCE, который необходим для быстрой сборки, отладки и
развертывания приложения:
```bash
    sudo apt install docker-ce install docker-ce docker-ce-cli containerd.io
```
После этого Docker будет установлен Также выше выполненная команда
запустит службу и настроит автозапуск процесса. Чтобы убедиться в том,
что служба работает, запросим её состояние:
```bash
    sudo systemctl status docker
```
Команда вернет следующую информацию:

    docker.service - Docker Application Container Engine
    Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
    Active: active (running) since Mon 2019-07-08 15:11:19 UTC; 58s ago
    Docs: https://docs.docker.com
    Main PID: 5709 (dockerd)
    Tasks: 8
    Memory: 31.6M
    CGroup: /system.slice/docker.service
    └─5709 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Теперь в системе работает сервис Docker и есть доступ к утилите
командной строки docker.

### Docker Compose

Средство, разработанное для помощи в определении и совместном использовании многоконтейнерных приложений(миниаркестратор). С помощью средства Compose можно создать файл YAML для определения служб и с помощью одной команды запускать и останавливать все, что необходимо.

Docker Compose можно установить из официальных репозиториев Debian, эти версии немного отстают от последней, поэтому в данном руководстве мы будем устанавливать Docker из репозитория GitHub. Будем используем флаг -o для указания выходного файла вместо переадресации вывода. Такой синтаксис позволяет избежать ошибки «отказ в разрешении» при использовании sudo.

Проверим текущий выпуск и при необходимости обновим его с помощью
следующей команды:
```bash
    sudo curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```
После этого мы настроим разрешения:
```bash
    sudo chmod +x /usr/local/bin/docker-compose
```
Проверим, что установка прошла успешно, с помощью проверки версии:
```bash
    docker-compose --version
```
В результате должна быть выведена установленная нами версия:

    Output
    docker-compose version 1.25.3, build d4d1b42b

2) Запускаем скрипт командой bash ./Docker_install.sh (дожидаемся окончания заугрузки и установки компонентов)

### Настройка Docker

По умолчанию команда docker требует привилегий root (или доступа к команде sudo). Также её можно запускать в группе `docker`, которая создаётся автоматически во время установки.

Если попытаться запустить команду `docker` без префикса `sudo` и вне группы docker, то получим ошибку:

    docker: Cannot connect to the Docker daemon. Is the docker daemon running on this host?.
    See 'docker run --help'.

Для того чтобы не пришлось набирать префикс `sudo` каждый раз, когда нам необходимо запустить команду docker, добавим своего пользователя в группу docker:
```bash
    sudo usermod -aG docker $USER
```
Чтобы активировать это изменение, выйдем из системы и войдём снова, или же введём:
```bash
    su - $USER
```
При этом будет запрошен пароль нашего пользователя.

Убедимся, что пользователь добавлен в группу:
```bash
    id -nG $USER
```
Чтобы добавить в группу `docker` пользователя, который не является текущим, укажем в команде его имя:
```bash
    sudo usermod -aG docker username
```
Docker установлен и готов к работе.



### Установка Docker Desctop
https://docs.docker.com/desktop/install/linux-install/
https://docs.docker.com/desktop/install/ubuntu/

### Образы Docker

Контейнеры Docker запускаются из образов. По умолчанию образы Docker хранятся на Docker Hub – это реестр Docker или registry, поддерживаемый командой разработчиков проекта. Разместить свой образ на Docker Hub может любой пользователь, потому здесь можно найти образы для многих приложений.

Для того чтобы проверить работоспособность Docker, традиционно как и в начале изучения языков програмирования запросим контейнер hello-world c аналогичным выводом.
```bash
    docker run hello-world
```
Команда должна вернуть следующий результат:

    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    1b930d010525: Pull complete
    Digest: sha256:41a65640635299bab090f783209c1e3a3f11934cf7756b09cb2f1e02147c6ed8
    Status: Downloaded newer image for hello-world:latest
    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    ...

Изначально Docker не может локально найти образ hello-world, потому он загружает этот образ из Docker Hub. Затем он создает контейнер на основе этого образа и запускает приложение внутри контейнера.

### Запуск контейнера с помощью Docker Compose

В общедоступном реестре Docker, Docker Hub, содержится образ Hello World, используемый для демонстрации и тестирования. Это минимальная конфигурация, необходимая для запуска контейнера с помощью Docker: файл YAML, вызывающий один образ. Мы создадим эту минимальную конфигурацию для запуска нашего контейнера hello-world.

Вначале создадим директорию для файла YAML и перейдем в нее:
```bash
    mkdir hello-world
    cd hello-world
```
Затем создадим файл YAML:
```bash
    vim docker-compose.yml
```
Поместим в файл следующие данные, сохраним его и закроем текстовый редактор:

```docker-compose.yml
  version: 3
  services:
    my-test:
     image: hello-world
```

Первая строка файла YAML используется в качестве части имени контейнера.
Вторая строка указывает, какой образ используется для создания контейнера. При запуске команды `docker-compose up` она ищет локальный образ по заданному нами имени hello-world. После этого можно сохранить и закрыть файл.

Имеется возможность вручную просмотреть образы в нашей системе с помощью команды docker images:
```bash
    docker images
```

Оставаясь в директории \~/hello-world, запустим следующую команду:
```bash
    docker-compose up -d
```
Если при первом запуске этой команды локальный образ с именем hello-world отсутствует, Docker Compose извлечет его из публичного репозитория на Docker Hub:

    Output
    Pulling my-test (hello-world:)...
    latest: Pulling from library/hello-world
    9db2ca6ccae0: Pull complete
    Digest: sha256:4b8ff392a12ed9ea17784bd3c9a8b1fa3299cac44aca35a85c90c5e3c7afacdc
    Status: Downloaded newer image for hello-world:latest
    . . .

После загрузки образа docker-compose создает контейнер, помещает в него и запускает программу hello, что, в свою очередь, подтверждает, что установка, выполнена успешно:

    Output
    . . .
    Creating helloworld_my-test_1...
    Attaching to helloworld_my-test_1
    my-test_1 |
    my-test_1 | Hello from Docker.
    my-test_1 | This message shows that your installation appears to be working correctly.
    my-test_1 |
    . . .

Затем развернутый в контейнере сервис выводит информацию о выполняемых процессах:

    Output
     To generate this message, Docker took the following steps:
    my-test_1  |  1. The Docker client contacted the Docker daemon.
    my-test_1  |  2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    my-test_1  |     (amd64)
    my-test_1  |  3. The Docker daemon created a new container from that image which runs the
    my-test_1  |     executable that produces the output you are currently reading.
    my-test_1  |  4. The Docker daemon streamed that output to the Docker client, which sent it
    my-test_1  |     to your terminal.

Контейнеры Docker продолжают работать, пока мы их не остановим, либо пока будет выполнятся какой либо процесс внутри него, поэтому после завершения работы hello контейнер останавливается. Соответственно этому, при просмотре активных процессов, заголовки столбцов будут отображаться, но контейнер hello-world не будет отображаться в списке, потому что он не запущен:
```bash
    docker ps
```
    Output
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES

Чтобы посмотреть информацию о контейнере, которая потребуется на
следующем шаге, используем флаг -a. Эта команда выводит все контейнеры, а не только запущенные:
```bash
    docker ps -a
```
    Output
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
    06069fd5ca23        hello-world         "/hello"            35 minutes ago      Exited (0) 35 minutes ago                       hello-world_my-test_1

Эта команда выводит информацию о всех контейнерах.

### Удаление образа

Чтобы избежать необязательного использования дискового пространства, мы удалим локальный образ. Для этого нам потребуется удалить все контейнеры, которые содержат образ, с помощью команды docker rm, после которой следует CONTAINER ID или NAME. В следующем примере мы используем идентификатор CONTAINER ID из команды `docker ps -a`, которую мы только что запускали. Обязательно заменим идентификатор на идентификатор нашего контейнера:
```bash
    docker rm 06069fd5ca23
```

##### Пройдите регистрацию на docker hub, запомните логин и пароль
выполните команду `docker login` для входа в реестр Docker

##### Для более быстрой установки создадим скрипт установки Docker
   - с помощью текстового редактора создаем файл Docker_install.sh
   - внесесм в него следующий список комманд 


```bash
#!/bin/bash
sudo apt update -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce -y
sudo systemctl start docker
sudo systemctl enable docker

sudo curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version
sudo usermod -aG docker $USER
reboot
```


Практические задания:
* Установите Docker на свою операционную систему https://docs.docker.com/engine/install/.
* Создайте скрипт пошаговой установки Docker для своей системы, с целью дальнейшей автоматизации процесса. (Сохраните его для следующих занятий).
* Установите Docker внутрь минимального образа `alpine`.

Возможные ошибки

ERROR: Couldn't connect to Docker daemon at http+docker://localhost - is it running? If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.

`maksimtechno@maksimtechno-VirtualBox:~/bit$ sudo chown $USER /var/run/docker.sock`


### Установка podman, как альтернатива docker 

`sudo apt install podman-docker`


Выполним команду
`systemctl --user enable --now podman.socket`

`settings -> парфметры -> 'введем' docker.host`

И установим следующее значени 
`unix:///run/user/1000/podman/podman.sock`