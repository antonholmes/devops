#### Lab 4. Dockerfile

Docker может автоматически создавать образы, читая инструкции из Dockerfile. Dockerfile - это текстовый документ, содержащий все команды, которые пользователь может вызвать в командной строке для сборки изображения. 


Рассмотрим формат Dockerfile
```Dockerfile
# Comment
INSTRUCTION arguments
```
В Dockerfile регистр не учитывается. Однако по соглашению инструкции (INSTRUCTION) должны быть ЗАГЛАВНЫМИ, чтобы легче отличать их от аргументов.

Docker запускает инструкции в Dockerfile по порядку. Dockerfile должен начинаться с инструкции FROM. Данная инструкция определяет родительский образ, из которого вы строите собственный. FROM может предшествовать только одна или несколько инструкций ARG, которые объявляют аргументы, использующиеся в строках FROM в Dockerfile. Docker рассматривает строки, начинающиеся с символа `#`, как комментарий, если только эта строка не является допустимой директивой парсера.

Рассмотрим наиболее часто использующиеся конструкции:


FROM

    Инструкция FROM определяет базовое изображение. 
    Данная инструкция должна быть первой в файле Dockerfile.

MAINTAINER

    Инструкция MAINTAINER позволяет вам задать поле Автора. 

LABEL

    Инструкция LABEL позволяет указать информацию метаданных для изображения.

ADD

    Инструкция ADD используется для копирования файлов, каталогов и удаленных URL-адресов в место назначения (контейнер docker) в файловой системе образов Docker.
    Если источником является сжатый файл (tar, gzip, bzip2 и т.д.), То он будет извлечен в указанном месте назначения.

COPY

    Инструкция КОПИРОВАНИЯ используется для копирования файлов, каталогов и удаленных URL-адресов в пункт назначения (контейнер docker) в файловой системе образов Docker.

RUN

    Инструкция RUN используется для выполнения любых команд поверх текущего изображения, и это создаст новый слой.

CMD

    Команда CMD используется для задания команды, которая будет выполняться при запуске контейнера. В файле настройки должен быть только один CMD.

ENTRYPOINT

    Инструкция ENTRYPOINT используется для настройки и запуска контейнера в качестве исполняемого файла.

VOLUME

    Инструкция VOLUME используется для создания подключения тома к контейнеру docker из файловой системы узла docker.

ENV

    Инструкция VOLUME используется для задания переменных среды с ключом и значением.

USER

    Инструкция VOLUME определяет имя пользователя, имя группы, идентификатор UID и идентификатор GUID для выполнения последующих команд.

WORKDIR

    Инструкция VOLUME указывает рабочий каталог.

ARG

    Инструкция ARG также используется для задания переменных среды с ключом и значением. Но эта переменная будет установлена только во время создания изображения, а не в контейнере.

EXPOSE

    Инструкция EXPOSE используется для установки сетевых портов, которые контейнер прослушивает во время выполнения


##### Docker образ для index.html

Выполним первую микрозадачу. 
Создадим образ nginx и пробросим в него index.html
Создадим Dockerfile

```Dockerfile
FROM nginx:alpine
ENV WORKDIR /usr/share/nginx/html

WORKDIR ${WORKDIR}

COPY index.html index.html
```

Создадим index.html
```html
<!doctype html>
<html>
  <head>
    <title>This is the title of the webpage!</title>
  </head>
  <body>
    <p>My one Dockerfile!!</p>
  </body>
</html>

```

Соберем образ, состоящий из index.html и nginx
`docker build . -t localhost:5000/nginx:0.0.1`
Запустим образ
`docker run -itd -p 8011:80 --name nginx-dock localhost:5000/nginx:0.0.1`

Перейдем на страницу и убедимся работает ли наш контейнер.
`http://localhost:8011/`
Мы создали первое приложение, основанное на микросервисных технологилиях.
Посмотрим логи контейнера
`docker logs nginx-dock`

Используя `docker build`, пользователи могут создать автоматизированную сборку, которая последовательно выполняет несколько инструкций командной строки.

Команда `docker build` создает образ из файла Docker, сборка обсуществляется рекурсивно. В этом примере показана команда сборки, которая использует текущий каталог (.):
`docker build .`

Традиционно Dockerfile находится в корне проекта. Используя флаг -f с docker build, можем указать путь к Dockerfile.
`docker build -f /path/to/a/Dockerfile .`

Вы можете указать тег, с которым будет сохранен новый образ, если сборка завершится успешно:

`docker build -t test/myapp .`

[Документация docker build](https://docs.docker.com/engine/reference/commandline/build/)

##### Veu.js
Выполним задание чуть сложнее. 
Создадим проект на Veu.js, упакуем в Docker образ и пробросим пробросим весь проект через nginx, загрузим на gitlab.

Разобьем на подзадачи:

Установим vue:

Vite - это инструмент для быстрого запуска проекта на основе базового шаблона для популярных фреймворков.
```
sudo apt install vite
```

Установим npm и nodejs
```
sudo apt install npm
```

Вывод должен быть следующим
```
xub1@xub1:~/project/devops$ node -v
v17.0.1
xub1@xub1:~/project/devops$ npm -v
8.1.0
```

Проиницилизируем проект на vuejs:
```bash
npm init vite@latest testvue --template vue
# выберите vue ->vue
cd testvue
npm install
npm run dev
```

Cоздадим Dockerfile:
```Dockerfile
FROM node:lts-alpine as build-stage
RUN apk update && apk upgrade
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

Соберем образ:
`docker build -t testvuedeploy .`
Запустим образ:
`docker run -itd -p 8002:80 --name testvuedeploy testvuedeploy`


```bash
#Удалим, все лишнее
docker system prune -a

git init
git add .
git commit -m 'testvuedeploy'

git remote add origin git@gitlab.com:antonholmes/veu-test-deploy-docker
git push --set-upstream origin master
```

```bash
#Проверьте количество веток 

#Проверим конект
echo 123 > README.md 
git add .
git commit -m 'test'
git push
```

*Практические задания*

1) Создайте одностраничный сайт(используйте любой стек технологий), пользуйтесь при этом системой контроля версий Git.
2) Создайте для него Dockerfile, запустите.



