#### Lab 5. Volume

##### Теоретические сведения

Для управления данными приложения, необходимо использовать память хостовой системы. Рассмотрим существующие подходы к монтированию:
* Volume(тома) хранятся в части файловой системы хоста, управляемой Docker (/ var / lib / docker / volume / в Linux). Процессы, не относящиеся к Docker, не должны изменять эту часть файловой системы. Тома - предпочтительный способ сохранить данные в Docker.

* Bind(привязка существующих каталогов внутрь контейнеров). Данный подход к монтированию может хранить данные в любом месте хост-системы. Процессы, не относящиеся к Docker, на хосте Docker или в контейнере Docker могут изменять их в любое время.

* Tmpfs mounts хранятся только в временной памяти хоста и никогда не записываются в его файловую систему.


Подробнее о предназначении, представленных типов монтирования:
##### Volume
Volume можно создать явно, используя команду `docker volume create`. В другом случае Docker может создать том во время создания контейнера или службы.

Когда вы создаете том, он сохраняется в каталоге на хосте Docker. Когда вы монтируете том в контейнер, именно этот каталог монтируется в контейнер. Это похоже на то, как работает монтирование в Linux, за исключением того, что тома управляются Docker и изолированы от основных функций хост-компьютера.

Данный том может быть использован одновременно в несколько контейнеров. Когда ни один из запущенных контейнеров не использует том, том по-прежнему доступен в Docker и не удаляется автоматически. Вы можете удалить неиспользуемые тома с помощью `docker volume prune`.

Когда вы монтируете том, он может быть именованным или анонимным. Анонимным томам не дается явное имя при первом подключении к контейнеру, поэтому Docker дает им случайное. Volume также поддерживают использование драйверов томов, которые, помимо других возможностей, позволяют хранить данные на удаленных хостах или у облачных провайдеров.

Кейсы для использования Volume:

* Обмен данными между несколькими запущенными контейнерами. Если вы не создаете его явно, том создается при первом монтировании в контейнер. Когда этот контейнер останавливается или удаляется, том все еще существует. Несколько контейнеров могут монтировать один и тот же том одновременно, как для чтения-записи, так и только для чтения. Тома удаляются только тогда, когда вы их явно удаляете.

* Когда хосту Docker не гарантируется наличие заданного каталога или файловой структуры. Тома помогают отделить конфигурацию хоста Docker от среды выполнения контейнера.

* Если вы хотите хранить данные своего контейнера на удаленном хосте или у облачного провайдера, а не локально.

* Когда вам нужно создать резервную копию, восстановить или перенести данные с одного хоста Docker на другой, тома - лучший выбор. Вы можете остановить контейнеры, используя том, а затем создать резервную копию каталога тома (например, /var/lib/docker/volume /volume-name).

* Когда вашему приложению требуется высокопроизводительный ввод-вывод на рабочем столе Docker. Тома хранятся в виртуальной машине Linux, а не на хосте, что означает, что операции чтения и записи имеют гораздо меньшую задержку и более высокую пропускную способность.

* Когда вашему приложению требуется полностью собственное поведение файловой системы на Docker Desktop. Например, ядру базы данных требуется точный контроль над очисткой диска, чтобы гарантировать надежность транзакций. Тома хранятся в виртуальной машине Linux и могут гарантировать эти гарантии, в то время как монтирование привязки удалено в macOS или Windows, где файловые системы ведут себя несколько иначе.


##### Bind mounts
 
Bind mounts имеют ограниченную функциональность по сравнению с томами. Когда вы используете данный тип монтирования, файл или каталог на хост-машине монтируется в контейнер. На файл или каталог ссылается его полный путь на хост-машине. Файл или каталог не обязательно должен существовать на хосте Docker. Он создается по запросу, если его еще нет. Bind mounts очень производительны, но они зависят от файловой системы хоста, имеющей определенную структуру каталогов.

Кейсы для использования Bind mounts:

* Совместное использование файлов конфигурации с хост-машины в контейнеры. Docker по умолчанию обеспечивает разрешение DNS для контейнеров, монтируя /etc/resolv.conf с хост-машины в каждый контейнер.

* Совместное использование исходного кода или артефактов сборки между средой разработки на хосте Docker и контейнером. Например, вы можете смонтировать целевой каталог Maven / в контейнер, и каждый раз, когда вы создаете проект Maven на хосте Docker, контейнер получает доступ к восстановленным артефактам.

* Если вы используете Docker для разработки таким образом, что ваш рабочий Dockerfile будет копировать готовые артефакты непосредственно в образ, а не полагаться на Bind.

##### Bind mounts
Монтирование tmpfs не сохраняется на диске ни на хосте Docker, ни в контейнере. Он может использоваться контейнером в течение всего жизненного цикла контейнера для хранения непостоянного состояния или конфиденциальной информации. Например, внутри служб swarm используется tmpfs mounts для монтирования секретов в контейнеры службы.

Tmpfs mounts лучше всего использовать в случаях, когда вы не хотите, чтобы данные сохранялись ни на хосте, ни в контейнере. Это может быть сделано из соображений безопасности, например при использовании секретов.

##### Пример команд

Ознакомимся с volume
`docker volume --help`
`docker volume create --help`

Пример 1. (Посмотрим какие volume содержатся в стандартном образе mongodb)

```bash
#Удаляем все неиспользуемые volume
docker volume prune -f

#Скачиваем образ с MongoDB
docker image pull mongo

# Смотрим всю информацию о скаченом образе, обращаем внимание на имеющиеся Volume внутри контейнера
docker image inspect mongo

            "Volumes": {
                "/data/configdb": {},
                "/data/db": {}
            }
# Отформатируем вывод, для получения инфомации только о Volume
docker image inspect mongo --format "{{.Config.Volumes}}"
```

Пример 2. Создание Volume

```bash

# Удаляем все неиспользуемые volume
docker volume prune -f

# Создаём Volume
docker volume create storage
docker volume create db

# Смотрим список Volume
docker volume ls

# Запустим контейнер с монго
docker container run -d -v storage:/data/db --name mongo_test mongo

# Смотрим список Volume, обращаем внимание, что создался дополнительный Volume
docker volume ls

# Проинспектируем
docker container inspect mongo_test 

        "Mounts": [
            {
                "Type": "volume",
                "Name": "e46064eb3b15f249ff7dddfcdffcf288e5bb7b0b133f568aa3ae2cabe8be8cc6",
                "Source": "/var/lib/docker/volumes/e46064eb3b15f249ff7dddfcdffcf288e5bb7b0b133f568aa3ae2cabe8be8cc6/_data",
                "Destination": "/data/configdb",
                "Driver": "local",
                "Mode": "",
                "RW": true,
                "Propagation": ""
            },
            {
                "Type": "volume",
                "Name": "storage",
                "Source": "/var/lib/docker/volumes/storage/_data",
                "Destination": "/data/db",
                "Driver": "local",
                "Mode": "z",
                "RW": true,
                "Propagation": ""
            }
        ]
#  Видим, что созданный как и в ручную, так и автоматически volume, прикручены к контейнеру
```

Пример 3. Выполним правильное монтирование

```bash
# Удаляем все не используемые volume, остановим контейнер, удалим контейнер
docker volume prune -f
docker container stop mongo_test
docker container rm mongo_test

# Создаём Volume
docker volume create storage
docker volume create config

# Запусти контейнер, прикрутим Volume, пробросим порты
docker container run -d -v storage:/data/db -v config:/data/configdb -p 27018:27017 --name mongo_test mongo

# Проинспектируем, и видим, что все созданные вручную Volume внутри контейнера
docker container inspect mongo_test 
```

Пример 4. Примонтируем volume к другому контейнеру

```bash
# Удаляем все неиспользуемые volume
docker volume prune -f
# Запустим вторую mongo
docker container run -d -v storage:/data/db -v config:/data/configdb -p 27019:27017 --name mongo_test1 mongo
#Просмотрим запущенные контейнеры
docker container ps
# Контейнер не будет запущен так как Volume уже используется
# При остановке уже запущенного ранее контейнера, созданный контейнер из данного примера запустится
```

Пример 5. Установка label 
label механизм применения метаданных к объектам Docker.
Вы можете использовать label для организации работы с image, записи информации о лицензировании, аннотирования взамосвязи между контейнерами, томами, сетями и другими объектами.

[Документация Label](https://docs.docker.com/config/labels-custom-metadata/)
```bash
# Удаляем все неиспользуемые volume
docker volume prune -f

# Создаём Volume
docker volume create --name storage --label maintainer='Ivanov I.I.' --label used_for='mongo db storage'

# Просмотрим установленные label
docker volume inspect storage
```

Пример 6. Манипулирование данными

```bash
# Удаляем все неиспользуемые volume
docker volume prune -f

# Создадим драйвер
docker volume create --name storage

# Запустим контейнер
docker container run -d -v storage:/data/db -v config:/data/configdb -p 27018:27017 --name mongo_test mongo

# Проинспектируем
docker volume inspect storage
# Видим точкумонтирования "Mountpoint": "/var/lib/docker/volumes/storage/_data"

# Скопируем данные внутрь volume
cp index.html /var/lib/docker/volumes/storage/_data

# Просмотрим содержимое каталога
sudo ls -l /var/lib/docker/volumes/storage/_data
```

Пример 7. Создание volume c опциями

```bash
# Удаляем все неиспользуемые volume
docker volume prune -f

# Создаём Volume
docker volume create --driver local \
    --opt type=tmpfs \
    --opt device=tmpfs \
    --opt o=size=100m \
    storage

# где опция --driver указывает название драйвера хранения. 
# Например двайвер облачного провайдера --driver dostorage.
# Остальные параметры являются опционными

# Запустим контейнер
docker container run -d -v storage:/data/db --name mongo mongo

# Параметр -v является сокращением от --volume
# docker container run -d --volume storage:/data/db --name mongo mongo
```

Пример 8. Также можно примонтировать volume, при помощи команды --mount. Предпочтительней --volume

```bash
# Удаляем контейнер
docker container rm -f webhost

# Удаляем все неиспользуемые volume
docker volume prune -f

docker container run -d --mount source=storage,target=/usr/share/nginx/html --name webhost nginx:alpine
```

Пример 9. Монтирование каталога файловой системы хоста в режиме только для чтения.
Предварительно создайте в текущем каталоге, каталог html с index.html

```bash
# Удаляем все не используемые volume
docker volume prune -f

docker container run -d \
    --mount type=bind, source="$(pwd)"/html,target=/usr/share/nginx/html,readonly \
    --publish 80:80 \
    --name webhost nginx:alpine
# где source источник монтирования, target место монтирования внутри контейнера
```

Пример 10. Пример монтирования нескольки файлов.
Предварительно создайте в текущей директории index.html about.html

```bash
# Удаляем контейнер
docker container rm -f webhost

# Удаляем все неиспользуемые volume
docker volume prune -f

docker container run -d \
    --mount type=bind,source="$(pwd)"/index.html,target=/usr/share/nginx/html/index.html \
    --mount type=bind,source="$(pwd)"/about.html,target=/usr/share/nginx/html/about.html \
    --publish 80:80 \
    --name webhost nginx:alpine

# For windows
docker container run -d \
    --mount type=bind,source=/"$(pwd)"/index.html,target=/usr/share/nginx/html/index.html \
    --mount type=bind,source=/"$(pwd)"/about.html,target=/usr/share/nginx/html/about.html \
    --publish 80:80 \
    --name webhost nginx:alpine
```                             

Пример 11. Пример создания tmpfs, типа volume(используется для секретов)

```bash
# Удаляем контейнер
docker container rm -f webhost

# Удаляем все не используемые volume
docker volume prune -f

docker run -d \
  -it \
  --name webhost \
  --mount type=tmpfs,destination=/app,tmpfs-size=100m \
  nginx:alpine                         
```

Пример 12. Не забываем html в текущей директории

```Dockerfile
FROM nginx:alpine

WORKDIR /usr/share/nginx/html

VOLUME /usr/share/nginx/html

COPY index.html index.html
# Результатом этого Dockerfile является образ, который указывает docker run создать новую точку монтирования в /usr/share/nginx/html и скопировать файл index.html во вновь созданный том.
```

Соберйм образ 
```docker build . -t example_volume_dockerfile```


*Практические задания*
1. Примонтируйте volume в проект из предыдущей работы.
2. Повторите примеры для СУБД PostgeSQL, сгенерировав несколько произвольных таблиц.
3. Проинспектируйте docker контейнер из примера 12, на наличие volume, запустите.
4. Выполните пример 6 для nginx, предварительно узнав порт, скопируйте внутрь образа конфигурационный файл, а также измените стартовую страницу. Проверьте работоспособность испольщуя команду `curl`.

