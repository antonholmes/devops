#### YAML, Введение в Docker-compose

##### YAML
YAML — это язык для хранения информации в формате понятном человеку. Его название расшифровывается как, «Ещё один язык разметки». Однако, позже расшифровку изменили на — «YAML не язык разметки», чтобы отличать его от настоящих языков разметки.

Язык похож на XML и JSON, но использует более минималистичный синтаксис при сохранении аналогичных возможностей. YAML обычно применяют для создания конфигурационных файлов в программах типа Инфраструктура как код (Iac), или для управления контейнерами в работе DevOps.

Чаще всего с помощью YAML создают протоколы автоматизации, которые могут выполнять последовательности команд записанные в YAML-файле. Это позволяет вашей системе быть более независимой и отзывчивой без дополнительного внимания разработчика.

Если вы не хотите, чтобы при синтаксическом разборе файла YAML повторялись ошибки, вы всегда должны помнить следующее при работе с YAML:

* Вкладки НЕ допускаются в YAML. Вы должны использовать отступы.
* Хотя количество места не имеет значения, если отступ дочернего узла больше, чем отступ родительского узла, может возникнуть ошибка. Необходимо оставлять одинаковое количество пробелов.
* Между различными элементами YAML должен быть пробел.
* YAML чувствителен к регистру.
* YAML-файл должен заканчиваться расширениями вроде .yaml или .yml.
* YAML поддерживает кодировку UTF-8, UTF-16 и UTF-32.


YAML поддерживает однострочные комментарии . Его структура объясняется ниже с помощью примера:
```yaml
# this is single line comment.
```
YAML не поддерживает многострочные комментарии.
```yaml
# this
# is a multiple
# line comment
```

Разберем пример docker-compose с точки зрения структуры yaml документа, в смысл строк не вникаем.
```yaml
version: "3.9"  # optional since v1.27.0
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
  redis:
    image: redis
volumes:
  logvolume01: {}
```
1. Обратите внимание на строку `image: redis` запись вида `image:redis` недопустима;
2. Обратите внимание на строку `services:`. Данной строкой мы начинаем описывать объект сервисы, где вложены объекты `web` и `redis`. Это можно сразу определить так, как данные объекты расположены на одной линии. Обратите на это внимание.
3. При описании вложенных объектов используют два пробела.
```yaml
# Перед строкой web два пробела
services:
  web:
```
Данное правило необходимо строго соблюдать.

##### Основные конструкции YAML файла

Скаляры – это строки и числа, из которых состоят данные на странице. Проще говоря, это пары ключ-значение.
```yaml
build: .
image: redis
```

Элементы или элементы списка и коллекции – это строки, которые начинаются на одном уровне отступа, начиная с тире, за которым следует пробел.
```yaml
 volumes:
   - .:/code
   - logvolume01:/var/log
```

Словари, они же dictionaries, они же mappings, схожи со калярами и содержат пары ключ:значение и, в отличии от скаляров, которые являются элементарным типом — могут содержать вложенные элементы:

Словарь в YAML:
```yaml
---
key1: "value1"
key2:
  - value2
  - value3
```
И он же в JSON:
```json
{
    "key1": "value1",
    "key2": [
        "value2",
        "value3"
    ]
}
```

Рассмотрите наш пример со словарем и вложенными в него списками:
```yaml
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
```

##### Docker-compose

Compose - это инструмент для определения и запуска многоконтейнерных приложений Docker. В Compose вы используете файл YAML для настройки служб вашего приложения. C помощью одной команды вы сможете создавить и запускаете сразу несколько контейнеров из заранее прописанной конфигурации. 

В случае, если вы будете повторять команды убедитесь, что docker-compose установлен командой, `docker-compose --version`, если не установлен, обратитесь к первому практическому занятию.

```docker-compose

version: '3'
# Устанавливается версия docker-compose
services:
# Определяем блок сервисы, внутри которого устанавливаются названия всех приложений, которые мы хотим запустить под эгидой docker-compose
  app:
  # Название приложения
    build:
    # Блок сборки
      context: .
      # Указываем место, где находится Dockerfile
    ports:
    # Блок порты
      - 8080:80
      # Пробрасываем порты

```

Для сборки приложения из текущего каталога, создайте docker-compose.yml файл и поместите в него код выше, не забывая создать Dockerfile (воспользуйтесь Lab. 4.)


##### Go, Nginx

Выше, мы рассмотрели основную структуру yaml файла, несколько примеров docker-compose.yml.

Соберем простое go приложение в связке с веб сервером nginx.

Начнем, с добавления в наш docker-compose.yml nginx, который может в проекте выступать как в качестве веб-сервера, так и proxy.

```docker-compose
version: '3'
services:
  websrv:
    # используем уже готовый docker образ из github, где alpine обозначает последнюю версию
    image: nginx:alpine
```

Выполним команду `docker-compose up`.

Данная команда запускает контейнеры в режиме отладки.
Для выхода из режима выполните Cntrl+C, в таком случае наш контейней остановится.
Провери состояние контейнера `docker ps -a`

Выполните команду `docker-compose up -d`, позволяющую запустить контейнеры в фоновом режиме.

Проверим состояние контейнера `docker ps -a`

```
version: '3'
services:
  websrv:
    image: nginx:alpine
    ports:
    # пробрасываем порт
      - 80:80
    volumes:
    # пробрасываем конфиг файл, предварительно создав его по данному пути ./nginx/nginx.conf
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
    # пробрасываем каталог html  
      - ./html:/usr/share/nginx/html
```

Следующим шагом создадим приложение на Go.
Создаем файла main.go к корне, с следующим содержанием
```go
//main.go
package main
import (
  "fmt"
  "log"
  "net/http"
  "github.com/gorilla/mux"
)
func main() {
  router := mux.NewRouter()
  router.HandleFunc("/", DoHealthCheck).Methods("GET")
  log.Fatal(http.ListenAndServe(":8080", router))
}
func DoHealthCheck(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Hello, i'm a golang microservice")
  w.WriteHeader(http.StatusAccepted) //RETURN HTTP CODE 202
}
```

Создадим Dockerfile под приложение GO
```Dockerfile
FROM golang:1.12.7-alpine3.10 AS build
# Support CGO and SSL
RUN apk --no-cache add gcc g++ make
RUN apk add git
WORKDIR /go/src/app
COPY . .
RUN go get github.com/gorilla/mux
RUN GOOS=linux go build -ldflags="-s -w" -o ./bin/test ./main.go

FROM alpine:3.10
RUN apk --no-cache add ca-certificates
WORKDIR /usr/bin
COPY --from=build /go/src/app/bin /go/bin
EXPOSE 8080
ENTRYPOINT /go/bin/test --port 8080
```
Протестируем работу Dockerfile для go приложения

Соберем приложение
docker build -t hello_go .

Запустим приложение
docker run -itd -p 9999:8080 --name hello_go hello_go

Проветим, localhost:9999, если все работает останавливаем и удаляем контейнер 

```
docker stop hello_go
docker rm hello_go
```

На данном этапе у нас имеется docker-compose только с одним приложением nginx и работающее приложение на go.
Соеденим все в docker-compose

```docker-compose
version: '3'
services:
  websrv:
    image: nginx:alpine
    ports:
      - 80:80
    volumes:
      - ./html:/usr/share/nginx/html
  # создадим новый контейнер с именем app
  app:
  # выполним сборку образа из Dockerfile текущей директории
    build: .
  # пробросим содержимое каталога внутрь контейнера
    volumes:
      - .:/src
  # пробросим дефолтовый порт 
    ports:
      - 8080
```

Выполним сборку docker-compose build
Запустим контейнер docker-compose up
Данная сборка объединяет nginx и пролижение в один docker-compose файл и соединяет их сетевым мостом, приложения друг о друге не знают.

Выполним команду, заметим, что создалась сеть по названию вашей директории.
`docker network ls`
Проинспектируем
`docker network inspect lab_6_default`
Заметим, что приложения объеденены сетью.


Создадим каталог для nginx
в котором создадим Dockerfile
```
FROM nginx:latest
EXPOSE 80
COPY ./nginx.conf /etc/nginx/nginx.conf
```

и nginx.conf
```
#nginx/nginx.conf
events {
    worker_connections 1024;
}
http {
  server_tokens off;
  server {
    listen 80;
    root  /var/www;

    location / {
      index index.html;
    }


    location /api/ {
      proxy_set_header X-Forwarded-For $remote_addr;
      proxy_set_header Host            $http_host;
      proxy_pass http://app:8080/;
    }
  }
}
```

Отредактируем docker-compose
```
version: 3
services:
  app:
    build: .
    ports:
      - 8080
  nginx:
    build: ./nginx
    ports:
      - 80:80
    depends_on:
      - app
```

Выполним сборку docker-compose build
Запустим контейнер docker-compose up
Перейдите для проверки http://localhost/api/

*Практические задания*

1. Cоздайте простое приложение на Vuejs, проксируйте все запросы на веб сервер nginx. (Примеры в каталоге Lab_6)

[Пример с php](https://badcode.ru/docker-tutorial-dlia-novichkov-rassmatrivaiem-docker-tak-iesli-by-on-byl-ighrovoi-pristavkoi/#what_is_docker_compose)

[docker-compose документация](https://docs.docker.com/compose/compose-file/compose-file-v3/)
