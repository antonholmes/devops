#### Уменьшаем размер образа

Память и вообще общее количество потребляемых используемых ресурсов(практическия работа №3), всегда является критичным, в любых проектах. При периодическом просмотре, необходимо обращать внимание, на размер образов.

Рассмотрим один из самых неудачных пример сборки проекта на Django.
```
# Выполняя рядовую команду, о выводе всех образов `docker image ls`.
Обратим внимание, что один из образов занимает огромное количество памяти.

REPOSITORY                          TAG                IMAGE ID       CREATED         SIZE
document_app                        latest             baf923ca522c   6 months ago    4.91GB
```

```
# Выполним команду, послойного просмотра содержимого контенера.
# Образ очень велик.

docker history document_app:latest

IMAGE          CREATED        CREATED BY                                      SIZE      COMMENT
baf923ca522c   6 months ago   /bin/sh -c #(nop) COPY dir:a425bfea52b90bf57…   981MB     
f892b9d1e11a   6 months ago   /bin/sh -c pip install -r requirement.txt       2.79GB    
536f5ce355df   6 months ago   /bin/sh -c apt-get install ffmpeg libsm6 lib…   243MB     
1b4ac96a2cd2   6 months ago   /bin/sh -c apt-get update -y                    17.6MB    
2ba525a04176   6 months ago   /bin/sh -c #(nop) COPY file:cdb8386db04d6b54…   1.66kB    
aef7e1985235   6 months ago   /bin/sh -c #(nop)  ENV PYTHONDONTWRITEBYTECO…   0B        
729322fc1bad   6 months ago   /bin/sh -c #(nop)  ENV PYTHONUNBUFFERED== 1     0B        
3c02dba5390d   6 months ago   /bin/sh -c #(nop) WORKDIR /usr/src/app          0B        
a6a0779c5fb2   6 months ago   /bin/sh -c #(nop)  CMD ["python3"]              0B        
<missing>      6 months ago   /bin/sh -c set -ex;   wget -O get-pip.py "$P…   8.17MB    
<missing>      6 months ago   /bin/sh -c #(nop)  ENV PYTHON_GET_PIP_SHA256…   0B        
<missing>      6 months ago   /bin/sh -c #(nop)  ENV PYTHON_GET_PIP_URL=ht…   0B        
<missing>      6 months ago   /bin/sh -c #(nop)  ENV PYTHON_PIP_VERSION=21…   0B        
<missing>      6 months ago   /bin/sh -c cd /usr/local/bin  && ln -s idle3…   32B       
<missing>      6 months ago   /bin/sh -c set -ex   && wget -O python.tar.x…   55.8MB    
<missing>      6 months ago   /bin/sh -c #(nop)  ENV PYTHON_VERSION=3.9.5     0B        
<missing>      6 months ago   /bin/sh -c #(nop)  ENV GPG_KEY=E3FF2839C048B…   0B        
<missing>      6 months ago   /bin/sh -c apt-get update && apt-get install…   18MB      
<missing>      6 months ago   /bin/sh -c #(nop)  ENV LANG=C.UTF-8             0B        
<missing>      6 months ago   /bin/sh -c #(nop)  ENV PATH=/usr/local/bin:/…   0B        
<missing>      6 months ago   /bin/sh -c set -ex;  apt-get update;  apt-ge…   510MB     
<missing>      6 months ago   /bin/sh -c apt-get update && apt-get install…   146MB     
<missing>      6 months ago   /bin/sh -c set -ex;  if ! command -v gpg > /…   17.5MB    
<missing>      6 months ago   /bin/sh -c set -eux;  apt-get update;  apt-g…   16.5MB    
<missing>      6 months ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      6 months ago   /bin/sh -c #(nop) ADD file:1a1eae7a82c66d673…   114MB   

```
Рассмотрим самые распространенные практики грамотного подхода к сбору image docker.

Имеется Dockerfile проекта Flask, соберем данный образ и посмотрим сколько памяти занимает.

```Dockerfile
FROM python:3.10-slim-buster
RUN apt update -y && apt upgrade -y

WORKDIR /app
COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
COPY . . 
# перенес копи был перед WORKDIR /app
ENV FLASK_APP=app
ENV FLASK_DEBUG=1
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```

Выполним команду `docker images | grep flask`
Видим вывод. Данный объем image, критически нам не подходит.
flask-test                          latest             e6200627df79   24 seconds ago   173MB

Запустим контейней, и перейдем по адресу localhost:5002, убедимся, что все работает. Данный этап выполняйте после каждой сборки.

docker run -itd -p 5002:5000 --name flask-test flask-test
Остановим
docker stop flask-test
удалим
docker rm flask-test

Изменим базовый образ, на первой строчки Dockerfile

FROM python:3.10-slim-buster -> FROM python:3.10-alpine
и удалм вторую строку
В образе slim-buster, уже предустановлены некоторые компоненты python, которые нам не нужны

```Dockerfile
FROM python:3.10-alpine

WORKDIR /app
COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
COPY . . 
# перенес копи был перед WORKDIR /app
ENV FLASK_APP=app
ENV FLASK_DEBUG=1
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```
Проверяем объем image, и видим, что мы уменьшили наш image в два раза, не будем останавливаться и совершенствуем наш docker образ

docker images | grep flask
flask-test                          latest             01a992110949   7 minutes ago    84MB

Проверяем работоспособность, останавливаем и удаляем контейнер.

Следующим этапом исключим попадание кэша, который остается после установки зависимостей, внутрь образа.

```Dockerfile
FROM python:3.10-alpine

WORKDIR /app
COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
#Изменим строчку ниже, добавив --no-cache-dir
RUN pip3 install --no-cache-dir -r requirements.txt
COPY . . 
# перенес копи был перед WORKDIR /app
ENV FLASK_APP=app
ENV FLASK_DEBUG=1
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```

Получается следующий результат, хоть чуть чуть уменьшили образ, но все равно результат.
```
xub@xub:~/project/Incedos$ docker images | grep flask
flask-test                          latest             e45a85233670   7 minutes ago    81.8MB
```

Запустим и проверим, что все работает, остановим и удалим контейнер.

Попытаемся еще больше уменьшить размер образа, для этого напишем файл `.dockerignore`, который должен лежать в корне проекта.
dockerignore, как gitignore не допустает попадания в образ лишних файлов из репозитория.

```.dockerignore
/databases
/venv
/__pycache__
.env
.gitignore
.git
```

Пересоберем образ. Смотрим результат.

```
xub@xub:~/project/Incedos$ docker images | grep flask
flask-test                          latest             2a55c3186550   21 seconds ago      68.1MB
```
Видим, что мы сэкономили еще памяти.
Традиционно проверим, работоспособность.

##### Рассмотрим дополнительные особенности сборки приложений

######  1. Пользователь не долен быть root
Каждый из образов основывавется на ОС Linux и по умолчанию все действия происходят из под пользователя root, в связи с этим при сборке необходимо использовать обычного пользователя. Пример по созданию alpine.
```Dockerfile
FROM alpine
WORKDIR /newuser
RUN addgroup -S newgroup && adduser -D newuser -G newgroup
RUN chown -R newuser /newuser
USER newuser
RUN whoami
```

######  2. Использование переменных окружения 
_Dockerfile_

Переменные окружения похожи на переменные в языках программирования. Они существуют в рамках запущенной сессии командного интерпретатора, подгружаются туда во время его инициализации (но это не единственный путь их появления). Посмотреть установленные переменные можно командой env (environment).
В Dockerfile переменные окружения устанавливаются как ключ-значение.
`ENV <key>=<value>`

Приведенный ниже отрывок Dockerfile, показывает применение переменных окружения. Следует запомнить, что переменные окручения используются точно также как и в bash.

```
ENV DIRNAME=/name
ENV DIRPATH=/path
WORKDIR $DIRPATH/$DIRNAME
RUN pwd
```

_docker-compose.yml_

В docker-compose.yml файле переменные окружения извлекаются из файла .env, которых должен находиться рядом с docker-compose.yml

Создадим файл .env с следующим содержимым
`TAG=v1.5`
И docker-compose.yml
```
version: 3
services:
  web:
    image: webapp:${TAG}
```

Просмотреть какие значения передаются в docker-compose можно при помощи команды docker-compose config
```stdout
services:
  web:
    image: webapp:v1.5
version: '3.0'
```

Значения переменной передоваемой в оболочку имеет приоритет над значениями, указанными в файле.
`export TAG=v2.0`
```stdout
services:
  web:
    image: webapp:v2.0
version: '3.0'
```
Вы можете изменить путь к файлу среды, используя аргумент командной строки --env-file.
`docker-compose --env-file ./config/.env.dev up`

_Установка переменных окружения жестко в контейнер (не является безопасным)_

Существует возможность установить переменные среды при помощью ключа 'environment'.
```
web:
  environment:
    - DEBUG=1
```
Так, как контейнеров в приложении может быть множество возникает потребность разделения переменных оргужения по микросервисам.
```
web:
  env_file:
    - web-variables.env
```

######  3. Healthcheck

Healthcheck означает проверку работоспособности любого ресурса.
Одним из инструментов является утилита curl

Представим, что приложение включает в себя три микросервися. Приложение на flask, db, pgadmin
pgadmin работает на 80 порту своей машины. Проверим
curl 172.25.0.2:80

```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>Redirecting...</title>
<h1>Redirecting...</h1>
<p>You should be redirected automatically to target URL: <a href="/browser/">/browser/</a>.  If not click the link
```
Проверим приложение
curl 172.25.0.3:5000
```
  File "/usr/local/lib/python3.10/site-packages/sqlalchemy/engine/default.py", line 584, in connect
    return self.dbapi.connect(*cargs, **cparams)
  File "/usr/local/lib/python3.10/site-packages/psycopg2/__init__.py", line 122, in connect
    conn = _connect(dsn, connection_factory=connection_factory, **kwasync)
sqlalchemy.exc.OperationalError: (psycopg2.OperationalError) connection to server at "172.25.0.2", port 5432 failed: Connection refused
        Is the server running on that host and accepting TCP/IP connections?

(Background on this error at: https://sqlalche.me/e/14/e3q8)

-->
```
Видим, что выдает ошибку из-за того, что база не инициализорована
postges можно проверить при помощи логов
docker logs flask_postgres_1 
```
2021-12-08 05:34:10.758 UTC [1] LOG:  starting PostgreSQL 14.1 (Debian 14.1-1.pgdg110+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
2021-12-08 05:34:10.812 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
2021-12-08 05:34:10.812 UTC [1] LOG:  listening on IPv6 address "::", port 5432
```
Можно заметить, что время в СУБД работает некорректно, это может быть критично при Мониторинге и сборе логов.

*Практические задания*
1. Cоздайте простое приложение на flask, подключите mongodb;
2. Уменьшите размер образа;
3. Используйте пременные окружения, для коннекта к db;
4. Исключите root из Dockerfile;
5. Проверьте работоспособность сервисов.
6. Запустите сборку приложения из 1 задания, в образе Dind и опубликуите полученные образы в публичном репозитории. Разверните приложение локально.  

Дополнительные источники
https://scoutapm.com/blog/how-to-use-docker-healthcheck
https://docs.docker.com/develop/develop-images/multistage-build/
https://sysdig.com/blog/dockerfile-best-practices/
https://testdriven.io/blog/docker-best-practices/
https://dev.to/chrisedrego/21-best-practise-in-2021-for-dockerfile-1dji