## Docker Swarm

Добро пожаловать на лекцию по Docker Swarm! В этой лекции мы поговорим о Docker Swarm - инструменте для управления контейнерами Docker в кластерной среде. Мы рассмотрим, что такое Docker Swarm, его основные компоненты, преимущества и способы использования.

Что такое Docker Swarm и чем он может быть полезен

При построении крупной продакшн-системы в нее обязательно закладывают требования по отказоустойчивости, производительности и масштабируемости. То есть система должна быть защищена от сбоев, не тормозить и иметь возможность для увеличения мощности.

Обычно для этого создают кластер — отдельностоящие хосты (серверы) объединяют под общим управлением, со стороны это выглядит как единая система. При этом она намного устойчивее к сбоям и производительнее:

    Отказоустойчивость достигается благодаря избыточности хостов (в рамках кластера они называются нодами). Система работает сразу на нескольких нодах, если одна из них выйдет из строя, остальные спокойно продолжат работу.
    Балансировка нагрузки позволяет равномерно нагрузить каждую ноду. Кластер следит за нагрузкой и сам распределяет внутри себя задачи: одну программу запустит на одной ноде, другую программу — на другой.
    Масштабируемость помогает подстраивать производительность кластера под нагрузку. Если приложениям и сервисам не хватает ресурсов, можно быстро подключить дополнительные ноды.

При работе с контейнерами эти задачи решают системы оркестровки. Оркестровка — это управление и координация взаимодействия между контейнерами. Контейнеры запускаются на хостах, а хосты объединяют в кластер.

У Docker есть стандартный инструмент оркестровки — Docker Swarm Mode, или просто Docker Swarm. Он поставляется «из коробки», довольно прост в настройке и позволяет создать простой кластер буквально за минуту.

Кластер Swarm (Docker Swarm Cluster) состоит из нод, которые делят на два типа:

    Управляющая нода (Manager). Это нода, которая принимает запросы и распределяет задачи между всеми нодами в кластере. Менеджеров может (и должно) быть несколько, но среди них обязательно есть одна самая главная нода — лидер, который управляет всем кластером.
    Рабочая нода (Worker). Подчиненная нода, которая не принимает решений, а просто выполняет отправляемые ей задачи.


![vm_vs_container](./images/docker-swarm.webp)

В Docker Swarm вместо прямого использования контейнеров используются сервисы (Docker Swarm Service). Они похожи на контейнеры, но всё же это немного другое понятие.

Сервис — это что-то вроде уровня абстракции над контейнерами. В Swarm мы не запускаем контейнеры явно — этим занимаются сервисы. Для достижения отказоустойчивости мы лишь указываем сервису количество реплик — нод, на которых он должен запустить контейнеры. А Swarm уже сам проследит за тем, чтобы это требование выполнялось: найдет подходящие хосты, запустит контейнеры и будет следить за ними. Если один из хостов отвалится — создаст новую реплику на другом хосте.


Преимущества Docker Swarm:
- Простота установки и использования: Docker Swarm легко устанавливается и настраивается, и его можно использовать с минимальными знаниями Docker.
- Масштабируемость: Docker Swarm позволяет горизонтально масштабировать приложения, добавляя или удаляя рабочие узлы.
- Отказоустойчивость: Docker Swarm обеспечивает отказоустойчивость, позволяя автоматически восстанавливать контейнеры в случае сбоев.
- Легкое управление: Docker Swarm предоставляет простой интерфейс командной строки и веб-интерфейс для управления кластером и контейнерами.

Для создания кластера Docker Swarm необходимо выбрать один из хостов в качестве менеджера и запустить команду `docker swarm init`. После этого можно добавить рабочие узлы в кластер с помощью команды `docker swarm join`.

После создания кластера Docker Swarm можно использовать команды `docker service` для создания, масштабирования и управления контейнерами. Например, команда `docker service create` создает новый сервис, а команда `docker service scale` изменяет количество запущенных контейнеров в сервисе.

Docker Swarm автоматически выполняет балансировку нагрузки между рабочими узлами, распределяя контейнеры по доступным ресурсам. Он также предоставляет встроенную маршрутизацию, позволяющую обращаться к сервисам по имени и автоматически перенаправлять запросы на соответствующие контейнеры. Docker Swarm предоставляет встроенные инструменты для мониторинга и управления кластером. Например, можно использовать команду `docker node ls` для просмотра состояния узлов кластера или использовать инструменты сторонних разработчиков для визуализации и мониторинга кластера.

Одним из главных преимуществ Docker Swarm является его способность обеспечивать высокую доступность и отказоустойчивость приложений. Когда один из менеджеров кластера выходит из строя, другой менеджер автоматически заменяет его, сохраняя работоспособность и непрерывность работы приложений. Также Docker Swarm обеспечивает автоматическую репликацию контейнеров на разных рабочих узлах, чтобы в случае сбоя одного узла приложение продолжало работать на других узлах.

Docker Swarm позволяет легко масштабировать приложения горизонтально путем добавления или удаления рабочих узлов. Вы можете использовать команду `docker service scale` для изменения количества контейнеров, работающих в сервисе. Docker Swarm автоматически распределит нагрузку между доступными узлами, обеспечивая эффективное использование ресурсов.

Docker Swarm предоставляет механизмы для безопасного управления секретами и конфигурациями приложений. Вы можете использовать команду `docker secret` для сохранения и передачи конфиденциальных данных, таких как пароли и ключи, между контейнерами. Кроме того, Docker Swarm поддерживает использование конфигурационных файлов, которые могут быть применены к сервисам без перезапуска контейнеров.

Docker Swarm легко интегрируется с другими инструментами и сервисами, такими как Docker Compose, Kubernetes, Prometheus и многими другими. Вы можете использовать Docker Compose для определения и развертывания многоконтейнерных приложений в Docker Swarm. Кроме того, Docker Swarm может быть интегрирован с Kubernetes для создания гибридных кластеров или использования Kubernetes API для управления кластером Docker Swarm.

Docker Swarm является мощным инструментом для управления контейнерами Docker в кластерной среде. Он обеспечивает высокую доступность, отказоустойчивость и масштабируемость приложений, а также упрощает управление контейнерами и обеспечивает интеграцию с другими инструментами. Docker Swarm позволяет разрабатывать и развертывать сложные приложения с помощью контейнеризации, обеспечивая гибкость и эффективность в работе.

В Docker Swarm существует несколько способов запуска стека и сервисов. Вот некоторые из них:

1. Через команду `docker stack deploy`: Создайте файл `docker-compose.yml`, в котором определите сервисы и их параметры. Затем, используя команду `docker stack deploy`, запустите стек на Docker Swarm кластере. Например:
   
```bash
   $ docker stack deploy -c docker-compose.yml my-stack
```   

2. Через Docker Hub: Вы можете использовать файл `docker-compose.yml`, в котором определены сервисы и их параметры, и разместить его в публичном репозитории Docker Hub. Затем, используя команду `docker stack deploy`, вы можете запустить стек, указав имя репозитория и тег образа. Например:
   
```bash
   $ docker stack deploy -c user/repo:tag my-stack
```  

3. Через Docker Swarm API: Вы можете использовать Docker Swarm API для программного запуска стека и сервисов. Для этого вам потребуется отправить POST-запрос на URL `http://swarm-manager:2377/v1.40/services/create` с телом запроса, содержащим определение сервиса. Например, с использованием CURL:
   
```bash
   $ curl -X POST --header "Content-Type: application/json" --data @service-definition.json http://swarm-manager:2377/v1.40/services/create
```   

4. Через управляющий UI: Docker Swarm имеет встроенный управляющий UI, который позволяет вам создавать и управлять стеками и сервисами с помощью веб-интерфейса. Вы можете получить доступ к UI, открыв веб-браузер и перейдя по адресу `http://swarm-manager:2377`.

Cписок полезных команд Docker Swarm:

- `docker swarm init`: Инициализировать новый Docker Swarm кластер на текущем узле.
- `docker swarm join`: Присоединить узел к существующему Docker Swarm кластеру.
- `docker swarm leave`: Покинуть Docker Swarm кластер на текущем узле.
- `docker swarm deploy`: Развернуть сервисы в Docker Swarm кластере.
- `docker service create`: Создать новый сервис в Docker Swarm кластере.
- `docker service ls`: Показать список запущенных сервисов в Docker Swarm кластере.
- `docker service ps`: Показать список задач (тасков) для определенного сервиса.
- `docker service scale`: Изменить количество реплик сервиса в Docker Swarm кластере.
- `docker service update`: Обновить параметры сервиса в Docker Swarm кластере.
- `docker node ls`: Показать список узлов в Docker Swarm кластере.
- `docker node inspect`: Показать информацию о конкретном узле в Docker Swarm кластере.
- `docker network create`: Создать новую сеть в Docker Swarm кластере.
- `docker network ls`: Показать список сетей в Docker Swarm кластере.
- `docker stack deploy`: Развернуть стек сервисов в Docker Swarm кластере.
- `docker stack ls`: Показать список развернутых стеков в Docker Swarm кластере.
- `docker stack ps`: Показать список задач (тасков) для определенного стека.