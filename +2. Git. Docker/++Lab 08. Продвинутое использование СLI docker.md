# Управление и аркестрация контейниризированной виртуализации в АИС

#### Продвинутое использование docker через интерфейс командной строки 

В предыдущей части курса были рассмотрены основны работы с Docker. Следующая часть будет посвящена более углубленному изучению контейнерной виртуализации на основе Docker.

В настоящей практике в качетсве разминки и введения в следующую часть курса вернемся к известному нам Command line interface (CLI) и рассмотрим менее известные команды, но необходимые при решении более сложных задач. Примеры будут представлены на основе образа веб сервера nginx. 

Команда *docker pause* приостанавливает все процессы в указанных контейнерах. В Linux для этого используется контрольная группа заморозки. Традиционно при приостановке процесса используется сигнал SIGSTOP, наблюдаемый приостанавливаемым процессом.

```bash
$ docker run -itd --name alpine nginx:alpine 
$ docker pause alpine 
alpine
$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS                   PORTS     NAMES
710159e7534f   nginx:alpine   "/docker-entrypoint.…"   19 seconds ago   Up 18 seconds (Paused)   80/tcp    alpine
```

Команда *docker unpause* отменяет приостановку всех процессов в контейнере(ах). В Linux это делается с помощью cgroup заморозки.
```bash
$ docker unpause alpine
alpine

$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS     NAMES
710159e7534f   nginx:alpine   "/docker-entrypoint.…"   52 minutes ago   Up 52 minutes   80/tcp    alpine
```

Взглянем на вывод *docker info*. Команда отображает общесистемную информацию о состоянии и настройке Docker на хосте включая: версию ядра, количество контейнеров и образов являющимися уникальными, установленные плагины, используемый репозиторий конейнеров и т.д. Представленные сведения говорят нам в какой спецификации работает Docker на хосте.

```bash
$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  buildx: Docker Buildx (Docker Inc., v0.8.2)
  compose: Docker Compose (Docker Inc., v2.6.1)
Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.17
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: runc io.containerd.runc.v2 io.containerd.runtime.v1.linux
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 10c12954828e7c7c9b6e0ea9b0c02b01407d3ae1
 runc version: v1.1.2-0-ga916309f
 init version: de40ad0
 Security Options:
  apparmor
  seccomp
   Profile: default
 Kernel Version: 4.4.0-210-generic
 Operating System: Alpine Linux v3.16 (containerized)
 OSType: linux
 Architecture: x86_64
 CPUs: 8
 Total Memory: 31.42GiB
 Name: node1
 ID: Q2I5:3ROX:AU7L:T6QB:YCH7:NXRH:NJON:V6AS:HUZZ:JHA4:GUGF:D6XY
 Docker Root Dir: /var/lib/docker
 Debug Mode: true
  File Descriptors: 26
  Goroutines: 41
  System Time: 2023-03-29T03:23:03.896206725Z
  EventsListeners: 0
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: true
 Insecure Registries:
  127.0.0.1
  127.0.0.0/8
 Live Restore Enabled: false
 Product License: Community Engine
 ```

Часто возникают задачи мониторинга состояния контейнера на предмет использования ресурсов и производительности. Современные подходы к мониторингу различных инфраструктурых компонентов рекомендуют использовать такие ниструменты как Grafana и Prometeus, рассмотрение которых выходит за рамки курса. Но в Docker cуществуют встроенные команды позволяющие получить загрузку системы. 

### Мониторинг 
#### Logs

Команда *docker logs* отображает

```bash
$ docker logs web
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/03/31 10:36:59 [notice] 1#1: using the "epoll" event method
2023/03/31 10:36:59 [notice] 1#1: nginx/1.23.4
2023/03/31 10:36:59 [notice] 1#1: built by gcc 12.2.1 20220924 (Alpine 12.2.1_git20220924-r4) 
2023/03/31 10:36:59 [notice] 1#1: OS: Linux 4.4.0-210-generic
2023/03/31 10:36:59 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2023/03/31 10:36:59 [notice] 1#1: start worker processes
2023/03/31 10:36:59 [notice] 1#1: start worker process 31
2023/03/31 10:36:59 [notice] 1#1: start worker process 32
2023/03/31 10:36:59 [notice] 1#1: start worker process 33
2023/03/31 10:36:59 [notice] 1#1: start worker process 34
2023/03/31 10:36:59 [notice] 1#1: start worker process 35
2023/03/31 10:36:59 [notice] 1#1: start worker process 36
2023/03/31 10:36:59 [notice] 1#1: start worker process 37
2023/03/31 10:36:59 [notice] 1#1: start worker process 38
172.19.0.1 - - [31/Mar/2023:10:37:08 +0000] "GET / HTTP/1.1" 200 615 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0" "-"
2023/03/31 10:37:08 [error] 32#32: *1 open() "/usr/share/nginx/html/favicon.ico" failed (2: No such file or directory), client: 172.19.0.1, server: localhost, request: "GET /favicon.ico HTTP/1.1", host: "ip172-19-0-7-cgjb6og1k7jg00agii80-80.direct.labs.play-with-docker.com", referrer: "http://ip172-19-0-7-cgjb6og1k7jg00agii80-80.direct.labs.play-with-docker.com/"
172.19.0.1 - - [31/Mar/2023:10:37:08 +0000] "GET /favicon.ico HTTP/1.1" 404 153 "http://ip172-19-0-7-cgjb6og1k7jg00agii80-80.direct.labs.play-with-docker.com/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0" "-"
172.19.0.1 - - [31/Mar/2023:10:38:21 +0000] "GET / HTTP/1.1" 304 0 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0" "-"
172.19.0.1 - - [31/Mar/2023:10:38:22 +0000] "GET / HTTP/1.1" 304 0 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0" "-"
```

Из вывода видно, что отработал скрипт `docker-entrypoint.sh`, запускаемый при старте контейнера, запустился nginx и стали поступать первые запросы.

> Используя встроенные команды Linux вы можете дополнительно фильтровать вывод логов контейнера. Однако множество программных решений имеют встроенные средства логирования, к каким и относится nginx.

#### stdin stdout stderr 
Для просмотра стандартного ввода, вывода и ошибки терминала (или любой комбинации этих трех) в контейнере используем команду `docker attach`. Данная команда позволяет вам просматривать его текущий вывод или управлять им в интерактивном режиме, как если бы команды выполнялись непосредственно в вашем терминале.

Чтобы остановить контейнер, используйте CTRL-c, эта последовательность клавиш отправляет SIGKILL в контейнер. Если --sig-proxy имеет значение true (по умолчанию), CTRL-c отправляет SIGINT в контейнер. Если контейнер был запущен с параметрами -i и -t, вы можете отсоединиться от контейнера и оставить его работающим с помощью последовательности клавиш CTRL-p CTRL-q.

```bash
$ docker attach web
2023/03/31 10:31:52 [notice] 37#37: signal 28 (SIGWINCH) received
2023/03/31 10:31:52 [notice] 36#36: signal 28 (SIGWINCH) received
2023/03/31 10:31:52 [notice] 30#30: signal 28 (SIGWINCH) received
2023/03/31 10:31:52 [notice] 1#1: signal 28 (SIGWINCH) received
2023/03/31 10:31:52 [notice] 35#35: signal 28 (SIGWINCH) received
2023/03/31 10:31:52 [notice] 33#33: signal 28 (SIGWINCH) received
```

Существует еще одна удобная команда просмотра проброшенных портов *docker port*. Информацию о портах ранее получали при помощи команды *docker ps*, в котороя показывает список запущеныых контейнеров с основными опциями запуска. 

В выводе команды слева представлен порт внутри контейнера.

```bash
$ docker port alpine
80/tcp -> 0.0.0.0:81
```

Самая распространенная команда для мониторинга *docker stats*. Отображает статистику использования ресурсов контейнеров в реальном времени. Представим стандартный вывод команды.

```bash
$ docker stats
CONTAINER ID   NAME            CPU %     MEM USAGE / LIMIT     MEM %     NET I/O   BLOCK I/O     PIDS
fda55d27c14a   some-postgres   0.00%     43.79MiB / 31.42GiB   0.14%     0B / 0B   0B / 50.2MB   6
96b991b8683e   alpine          0.00%     9.25MiB / 31.42GiB    0.03%     0B / 0B   0B / 32.8kB   9
```
> Выход из интерактивного режима *Ctrl+C*

| Колонка                | Описание                                                    |
----------------------------|------------------------------------------------------
| CONTAINER ID and Name     | идетнификатор и название контенера |
| CPU % and MEM % | процент ЦП и памяти, которые использует контейнер на хосте |
| MEM USAGE / LIMIT |общий объем памяти, который использует контейнер, и общий объем памяти, который ему разрешено использовать |
| NET I/O | Объем данных, которые контейнер получил и отправил через свой сетевой интерфейс |
| BLOCK I/O | Объем данных, которые контейнер записал и прочитал с блочных устройств на хосте|
| PIDs | количество процессов или потоков, созданных контейнером |

Cущуствует возможность форматировать вывод команды при помощи параметра(--format), оставляя необходимые столбцы.

```bash
$ docker stats --format "{{.Container}}: {{.CPUPerc}}"

fda55d27c14a: 0.00%
96b991b8683e: 0.00%
```

Более подробная информация о форматировании представлена на сайте https://docs.docker.com/engine/reference/commandline/stats/.

> Также можем вывести информацию о статистике конкретного контейнера или нескольких контейнеров указав идентификатор или название.   

Минусом представленной команды является то, что она не отображает всю статистику использования контейнеров во временном диапазоне.

> Существует множество визуальных инструментов мониторинга контейнерных сред встроенных в Docker Desktop, Portainer и др. Инструменты сбора и анализа метрик производительности под ваши потребности, такие как Prometeus и Grafana. Более подробно об этих и других программмных решения в следующих курсах.

Benchmarking HTTP означает измерение производительности и эффективности веб-сервера или веб-приложения. Это позволяет оценить, насколько хорошо сервер обрабатывает запросы и отдает ответы, а также определить, какие изменения в конфигурации сервера или коде приложения могут улучшить его производительность.

Benchmarking HTTP может быть полезным для разработчиков и системных администраторов, чтобы оптимизировать работу веб-сервера или приложения, улучшить его отзывчивость и обработку запросов, а также определить предельные нагрузки, которые сервер может выдержать.

Существуют различные инструменты для проведения бенчмаркинга HTTP, такие как: ApacheBench, Siege, Gobench, Apache JMeter, wrk и другие. Они позволяют отправлять большое количество запросов на сервер и измерять время ответа, пропускную способность и другие метрики производительности.


#### Использование ApacheBench в Docker

Скачиваем образ
`docker pull jordi/ab`

Запустим бейнчмарк
`docker run --rm jordi/ab -k -c 100 -n 1000 http://localhost:85`

Папамерт _-с_ указывает на количество одновременных соединений _-n_ на количество requests запросов.

Когда параметр _-k_ указан, ab будет использовать HTTP keep-alive соединения при отправке запросов на сервер. Это означает, что после получения ответа от сервера, соединение не будет закрыто, а будет использовано для отправки следующего запроса. Это может быть полезно при бенчмаркинге, чтобы избежать накладных расходов на установление нового соединения для каждого запроса.

Хорошие показатели производительности могут отличаться в зависимости от конкретного приложения или ситуации, но в целом следующие показатели считаются хорошими:

1. Низкое время отклика (response time): Это время, которое требуется серверу для обработки запроса и отправки ответа. Чем меньше это время, тем быстрее сервер отвечает на запросы клиента.

2. Высокая пропускная способность (throughput): Это количество запросов, которые сервер может обработать за единицу времени. Чем выше пропускная способность, тем больше запросов может быть обработано, что означает более эффективную работу сервера.

3. Низкий уровень ошибок: В хорошо производительном приложении или сервере должно быть минимальное количество ошибок или сбоев, которые могут повлиять на работу и доступность сервиса.

4. Минимальное использование ресурсов: Хорошая производительность также означает эффективное использование ресурсов, таких как процессорное время, память и сетевая пропускная способность. Чем меньше ресурсов требуется для обработки каждого запроса, тем больше запросов может быть обработано.

5. Масштабируемость: Хорошая производительность также означает, что приложение или сервер может масштабироваться и обрабатывать большую нагрузку при увеличении количества пользователей или запросов.

#### Docker настройка

*Практические задания*
> Все полученные сведения полученные в результате выполнения практических заданий запишите в файл в текущей датой выполнения.

1. Запустите контейнер и посотрите информацию о состоянии docker на хосте. Выведите информацию о типе операционной системы на которой установлен docker в файл, этом используйте командку docker info. 
2. Запустите произвольный контейнер. Выполните тестирование производительности в течении 3 минут. Оцените общую производительность во время выполения теста системы. Заморозьте контейнер. Разморозьте контейнер. 
3. Посотрите логи веб сервера apache, когда контейнер находится под тестированием производительности.
4. Запустите еще один контейнер с mongodb. Посмотрите статистику всех конетйнеров, используя (--format), не включая столбец PIDS. Запишите все результаты в файл. 
5. Изучите дополнительные параметры в тестировании производительности веб приложений.
6. Используя контейнер nginx, послитайте количество запросов к веб серверу за один день. Представьте статистику стран, с которых осуществлялись запросы на веб сервер (предванительно запустите тесты несколько раз и с разных ip).   

![progress](http://www.yarntomato.com/percentbarmaker/button.php?barPosition=28&leftFill=%23FF0000 "progress") 

:smiley: 