### Безопасность контейнерных сред

Проверка контейнеров на наличие уязвимостей является важной задачей для обеспечения безопасности ваших приложений и инфраструктуры. Docker Scout - это инструмент, который помогает автоматизировать процесс сканирования контейнеров на наличие известных уязвимостей.

### Установка Docker Scout

Переходим в рездел, релиз и выбираем последнюю версию Docker Scout.
https://github.com/docker/scout-cli/releases/

Выбирает архив согласно вашей архитектуре процессора(в примере amd64). Скачивает командой `wget`

```bash
wget -O docker-csout.tar https://github.com/docker/scout-cli/releases/download/v1.4.1/docker-scout_1.4.1_linux_amd64.tar.gz
```

Разархивируем
```bash
tar -xvf docker-csout.tar
```

Создадим каталог `~/.docker/cli-plugins`, и перенесем `docker-scout` в данный каталог при помощи команды mv.

Для получения справкти выполните команду в терминале `docker-scout`

### Использование основынх команд Docker Scout

### Docker scout quickview
Команда быстрого просмотра `docker scout quickview` выводит краткую информацию о образе. Он отображает сводку уязвимостей в указанном образе и уязвимостях в базовом образе. Если доступно, он также отображает обновление базового образа и рекомендации по обновлению.

> Если docker образа нет на вашем хосте он, будет автоматически скачан.

```bash
docker scout quickview 451777/learngit
    ✓ Pulled
    ✓ Image stored for indexing
    ✓ Indexed 696 packages

  Target               │  451777/learngit:latest  │   12C    48H    33M     5L     1?   
    digest             │  80d5db01de0b            │                                     
  Base image           │  nginx:1-alpine          │    7C    21H    10M     1L     2?   
  Refreshed base image │  nginx:1-alpine          │    0C     0H     0M     0L          
                       │                          │    -7    -21    -10     -1     -2   
  Updated base image   │  nginx:1-alpine-slim     │    0C     0H     0M     0L          
                       │                          │    -7    -21    -10     -1     -2   

What's Next?
  View vulnerabilities → docker scout cves 451777/learngit
  View base image update recommendations → docker scout recommendations 451777/learngit
  Include policy results in your quickview by supplying an organization → docker scout quickview 451777/learngit --org <organization>
```

Результат выполнения команды показал что было найдено 12 критических уязвимосте, 48 с высоким потенциалов, 33 с средним и 5 с минимальным. Обратите внимание, что предложено углубленное сканарование командой `docker scout cves` и просмотреть рекомендации по изменению образа `docker scout recommendations`.

### Docker scout cves
Команда `docker scout cves` анализирует программный артефакт на наличие уязвимостей.

> Если образ не указан, `docker scout будет` использовать последний созданный образ.

Поддерживаются следующие типы исследуемых артефактов:
   - Образы;
   - Структуру хранения образов согласно OCI, в которую входят hash и другая метаинформация;
   - Архивы, созданные с помощью docker save;
   - Локальные каталоги и файлы.

Для сканирования перечиленных артефактов существуют особенные префиксы выполнения команды `docker scout`, которые можно найти в официальной документации https://docs.docker.com/engine/reference/commandline/scout_cves/.

Выполним команду `docker scout cves 451777/learngit`

Разультат показал более разширенный обзор результатов сканирования образа. Выявлены 40 уязимостей в пакетах с общем их количеством 93.
```bash
## Overview
 Detected 40 vulnerable packages with a total of 93 vulnerabilitie

                    │           Analyzed Image            
────────────────────┼─────────────────────────────────────
  Target            │  451777/learngit:latest             
    digest          │  80d5db01de0b                       
    platform        │ linux/amd64                         
    vulnerabilities │   12C    48H    33M     5L     1?   
    size            │ 59 MB                               
    packages        │ 696        
```

Следущая часть вывода этой же комнды показывает в чем конкрено заключается уязвимость и дает ссылку на ее описание. Вывод был представлен по всем 93 уязвимостям.
```bash
pkg:apk/alpine/curl@7.80.0-r0?os_name=alpine&os_version=3.15

    ✗ CRITICAL CVE-2023-38545
      https://scout.docker.com/v/CVE-2023-38545
      Affected range : <8.4.0-r0  
      Fixed version  : 8.4.0-r0   
    
    ✗ CRITICAL CVE-2022-32221
      https://scout.docker.com/v/CVE-2022-32221
      Affected range : <7.80.0-r4  
      Fixed version  : 7.80.0-r4   
    
    ✗ CRITICAL CVE-2022-32207
      https://scout.docker.com/v/CVE-2022-32207
      Affected range : <7.80.0-r2  
      Fixed version  : 7.80.0-r2 
```

### Docker scout recommendations

Команда рекомендаций `docker scout recommendations` анализирует образы и выводит рекомендации по их обновлениюндации по обновлению.  Для каждой рекомендации отображается список преимуществ, таких как меньшее количество уязвимостей или меньший размер изображения.

Выполним команду
```bash
docker scout recommendations 451777/learngit

 │ The base image is also available under the supported tag(s)  alpine ,  mainline-alpine . If you want to display 
  │ recommendations specifically for a different tag, please re-run the command using the  --tag  flag.             

Refresh base image
  Rebuild the image using a newer base image version. Updating this may result in breaking changes.


            Tag            │                        Details                         │    Pushed    │          Vulnerabilities            
───────────────────────────┼────────────────────────────────────────────────────────┼──────────────┼─────────────────────────────────────
   1-alpine                │ Benefits:                                              │ 3 months ago │    0C     0H     0M     0L          
  Newer image for same tag │ • Same OS detected                                     │              │    -7    -21    -10     -1     -2   
  Also known as:           │ • Minor runtime version update                         │              │                                     
  • 1.25.3-alpine          │ • Newer image for same tag                             │              │                                     
  • 1.25-alpine            │ • Tag was pushed more recently                         │              │                                     
  • alpine                 │ • Image introduces no new vulnerability but removes 39 │              │                                     
  • alpine3.18             │ • 1-alpine was pulled 278K times last month            │              │                                     
  • 1-alpine3.18           │                                                        │              │                                     
  • mainline-alpine        │ Image details:                                         │              │                                     
  • 1.25-alpine3.18        │ • Size: 18 MB                                          │              │                                     
  • 1.25.3-alpine3.18      │ • Flavor: alpine                                       │              │                                     
  • mainline-alpine3.18    │ • Runtime: 1.25.3                                      │              │                                             
```
> Из вывода видим рекомендации по изменению базового образа

Команда сравнения `docker scout compare` анализирует два образа и отображает сравнение.

Использование этой команды позволяет сравнивать две версии одной и того же образа.
> Используется для создания нового образа на базе предшествующего.
```bash
docker scout compare --to nginx:alpine nginx
```

### Docker scout compare

Обратите внимание, что Docker Scout использует уязвимости, определенные в базе данных уязвимостей, таких как NVD (National Vulnerability Database) и других. Регулярное сканирование контейнеров с использованием инструментов, таких как Docker Scout, является хорошей практикой для обнаружения и устранения уязвимостей в вашей инфраструктуре Docker.

https://docs.docker.com/engine/reference/commandline/scout/

#### Инструменты docker для уменьшения вектора атак

##### HADOLINT

Удобный линтер Dockerfile, который помогает создавать правильные Dockerfile, согласно лучших практик.

Скачаем image
`docker pull hadolint/hadolint`

Перейдите в каталог с вашим Dockerfile
`docker run --rm -i hadolint/hadolint < Dockerfile`
Перед вами два вывода
```bash
-:3 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
-:4 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
-:4 DL3059 info: Multiple consecutive `RUN` instructions. Consider consolidation.
-:4 DL3019 info: Use the `--no-cache` switch to avoid the need to use `--update` and remove `/var/cache/apk/*` when done installing packages
-:8 DL3059 info: Multiple consecutive `RUN` instructions. Consider consolidation.
-:11 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
-:15 DL3025 warning: Use arguments JSON notation for CMD and ENTRYPOINT arguments
```

```bash
-:2 DL3027 warning: Do not use apt as it is meant to be a end-user tool, use apt-get or apt-cache instead
-:8 DL3042 warning: Avoid use of cache directory with pip. Use `pip install --no-cache-dir <package>`
-:8 DL3059 info: Multiple consecutive `RUN` instructions. Consider consolidation.
```

##### Dive
Инструмент для изучения содержимого слоев образа Docker и поиска способов уменьшить его размер. 

Установка
```bash
wget https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.deb
sudo apt install ./dive_0.9.2_linux_amd64.deb
```

Запустим
```bash
dive flask_mainapp:latest


Cmp   Size  Command                               ├── bin
     69 MB  FROM a0441b36624a46d                  │   ├── bash
    7.0 MB  set -eux;  apt-get update;  apt-get i │   ├── cat           
     29 MB  set -ex   && savedAptMark="$(apt-mark │   ├── chgrp   
       0 B  cd /usr/local/bin  && ln -s idle3 idl │   ├── chmod        
    9.5 MB  set -ex;   savedAptMark="$(apt-mark s │   ├── chown       
     21 MB  apt update -y && apt upgrade -y       │   ├── cp        
       0 B  #(nop) WORKDIR /app                   │   ├── dash           
     780 B  #(nop) COPY file:dee2049857c0d2462dfd │   ├── date     
     15 MB  /usr/local/bin/python -m pip install  │   ├── dd                           
    129 MB  pip3 install -r requirements.txt      │   ├── df                       
     63 MB  #(nop) COPY dir:20622e83d6174117e1202 │   ├── dir                              
                                                  │   ├── dmesg                     
│ Layer Details ├──────────────────────────────── │   ├── dnsdomainname → hostname     
                                                  │   ├── domainname → hostname    
Tags:   (unavailable)                             │   ├── echo                           
Id:     4cd949ff443ee86d59b852cd897a3706fee36c0f7 │   ├── egrep                  
dcf81afb6f867b33d506679                           │   ├── false                    
Digest: sha256:ddc260ab913e1beb894c1ab211bec2b938 │   ├── fgrep                     
9dbb2d3536b96d43a46cf059c13b8e                    │   ├── findmnt                 
```

#### Security
Недавняя атака на инфраструктуру Kubernetes Tesla заставила всех понять, что переход от Bare-Metal-Machines к Virtual-Machines, вплоть до контейнеров, никогда не устраняет лазейки в безопасности, которые часто остаются без внимания. Что ж, есть несколько лучших практик с точки зрения безопасности, которым можно следовать при Dockerizing и приложениях, таких как забота о секретах / учетных данных, избегайте использования пользователя root в качестве пользователя по умолчанию для контейнера и нескольких других. Лучшим подходом к борьбе с уязвимостями безопасности в сфере контейнеров является включение инструментов / технологий, ориентированных на выполнение надежных проверок безопасности по отношению к контейнеру, который присутствует в вашей среде. Есть несколько инструментов, которые можно добавить в ваш арсенал безопасности.

##### Dockle
Dockle проверяет корректность и безопасность конкретного образа как такового, анализируя его слои и конфигурацию – какие пользователи созданы, какие инструкции используются, какие тома подключены, присутствие пустого пароля и т.д. Таким образом функционал частично пересекается с DBS, но позволяет сканировать конкретный образ и за счёт простоты – можно легко внедрить этот инструмент в CI/CD.

Скачиваем последнюю версию 
```bash
wget https://github.com/goodwithtech/dockle/releases/download/v0.4.3/dockle_0.4.3_Linux-64bit.tar.gz && tar zxf dockle_0.4.3_Linux-64bit.tar.gz 
```

Выполняем сканирование ./dockle incedos_fl:latest

```bash
FATAL   - CIS-DI-0010: Do not store credential in environment variables/files        * Suspicious filename found : usr/local/lib/python3.10/site-packages/pymongo/settings.py (You can suppress it with "-af settings.py")
FATAL   - DKL-DI-0005: Clear apt-get caches
        * Use 'rm -rf /var/lib/apt/lists' after 'apt-get install|update' : /bin/sh -c apt update -y && apt upgrade -y
WARN    - CIS-DI-0001: Create a user for the container
        * Last user should not be root
WARN    - DKL-DI-0006: Avoid latest tag
        * Avoid 'latest' tag
INFO    - CIS-DI-0005: Enable Content trust for Docker
        * export DOCKER_CONTENT_TRUST=1 before docker pull/build
INFO    - CIS-DI-0006: Add HEALTHCHECK instruction to the container image
        * not found HEALTHCHECK statement
INFO    - CIS-DI-0008: Confirm safety of setuid/setgid files
        * setgid file: grwxr-xr-x usr/bin/wall
        * setuid file: urwxr-xr-x usr/bin/passwd
        * setgid file: grwxr-xr-x usr/bin/expiry
        * setgid file: grwxr-xr-x usr/bin/chage
        * setuid file: urwxr-xr-x bin/su
        * setgid file: grwxr-xr-x sbin/unix_chkpwd
        * setuid file: urwxr-xr-x usr/bin/chsh
        * setuid file: urwxr-xr-x usr/bin/chfn
        * setuid file: urwxr-xr-x usr/bin/newgrp
        * setuid file: urwxr-xr-x bin/umount
        * setuid file: urwxr-xr-x bin/mount
        * setuid file: urwxr-xr-x usr/bin/gpasswd
INFO    - DKL-LI-0003: Only put necessary files
        * Suspicious directory : tmp 
        * unnecessary file : app/mongotest/docker-compose.yml 
        * unnecessary file : app/Dockerfile 
        * unnecessary file : app/docker-compose.yml
```

#####  Anchore
Сканер уязвимостей для образов контейнеров и файловых систем. Легко установите двоичный файл, чтобы опробовать его. Работает с Syft, мощным инструментом SBOM (спецификация программного обеспечения) для образов контейнеров и файловых систем.

Сканируйте содержимое образа контейнера или файловой системы, чтобы найти известные уязвимости.

```bash
docker pull anchore/grype

docker run -it --rm anchore/grype mongo

⠸ Vulnerability DB        ━━━━━━━━━━━━━━━━━━━━━━━  [86 MB / 90 MB]
 ⠴ Parsing image           ━━━━━━━━━━━━━━━━━━━━━━━

```output
 ✔ Vulnerability DB        [updated]
 ✔ Parsed image             ✔ Cataloged packages      [153 packages]
 ✔ Scanned image           [52 vulnerabilities]
NAME                  INSTALLED                 FIXED-IN  VULNERABILITY     SEVERITY   
bash                  5.0-6ubuntu1.1                      CVE-2019-18276    Low         
coreutils             8.30-3ubuntu2                       CVE-2016-2781     Low         
krb5-locales          1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
krb5-locales          1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libasn1-8-heimdal     7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2020-6096     Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2021-3326     Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2016-10228    Negligible  
libc-bin              2.31-0ubuntu9.2                     CVE-2021-35942    Medium      
libc-bin              2.31-0ubuntu9.2                     CVE-2021-33574    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2021-38604    Medium      
libc-bin              2.31-0ubuntu9.2                     CVE-2020-27618    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2021-27645    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2020-29562    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2019-25013    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2020-6096     Low         
libc6                 2.31-0ubuntu9.2                     CVE-2021-3326     Low         
libc6                 2.31-0ubuntu9.2                     CVE-2016-10228    Negligible  
libc6                 2.31-0ubuntu9.2                     CVE-2021-35942    Medium      
libc6                 2.31-0ubuntu9.2                     CVE-2021-33574    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2021-38604    Medium      
libc6                 2.31-0ubuntu9.2                     CVE-2020-27618    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2021-27645    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2020-29562    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2019-25013    Low         
libgmp10              2:6.2.0+dfsg-4                      CVE-2021-43618    Low         
libgssapi-krb5-2      1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libgssapi-krb5-2      1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libgssapi3-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libhcrypto4-heimdal   7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libheimbase1-heimdal  7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libheimntlm0-heimdal  7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libhx509-5-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libk5crypto3          1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libk5crypto3          1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libkrb5-26-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libkrb5-3             1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libkrb5-3             1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libkrb5support0       1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libkrb5support0       1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libpcre3              2:8.39-12build1                     CVE-2020-14155    Negligible  
libpcre3              2:8.39-12build1                     CVE-2017-11164    Negligible  
libroken18-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libsqlite3-0          3.31.1-4ubuntu0.2                   CVE-2020-9991     Low         
libsqlite3-0          3.31.1-4ubuntu0.2                   CVE-2020-9849     Low         
libsqlite3-0          3.31.1-4ubuntu0.2                   CVE-2020-9794     Medium      
libtasn1-6            4.16.0-2                            CVE-2018-1000654  Negligible  
libwind0-heimdal      7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
login                 1:4.8.1-1ubuntu5.20.04.1            CVE-2013-4235     Low         
passwd                1:4.8.1-1ubuntu5.20.04.1            CVE-2013-4235     Low         
perl-base             5.30.0-9ubuntu0.2                   CVE-2020-16156    Medium      
```

##### Docker Bench for Security
The Docker Bench for Security is a script that checks for dozens of common best-practices around deploying Docker containers in production. The tests are all automated, and are based on the CIS Docker Benchmark v1.3.1.

Для запуска утилиты воспользуйтесь следующим образом
```bash
docker run --rm --net host --pid host --userns host --cap-add audit_control \
    -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
    -v /etc:/etc:ro \
    -v /lib/systemd/system:/lib/systemd/system:ro \
    -v /usr/bin/containerd:/usr/bin/containerd:ro \
    -v /usr/bin/runc:/usr/bin/runc:ro \
    -v /usr/lib/systemd:/usr/lib/systemd:ro \
    -v /var/lib:/var/lib:ro \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --label docker_bench_security \
    docker/docker-bench-security
```
Утилита проверяет приложения по следующим параметрам:
- Host Configuration
- Docker daemon configuration
- Docker daemon configuration files
- Container Images and Build File
- Container Runtime
- Docker Security Operations
- Docker Swarm Configuration

```bash
# ------------------------------------------------------------------------------
# Docker Bench for Security v1.3.4
#
# Docker, Inc. (c) 2015-
#
# Checks for dozens of common best-practices around deploying Docker containers in production.
# Inspired by the CIS Docker Community Edition Benchmark v1.1.0.
# ------------------------------------------------------------------------------

Initializing Thu Dec  9 07:07:03 UTC 2021


[INFO] 1 - Host Configuration
[WARN] 1.1  - Ensure a separate partition for containers has been created
[NOTE] 1.2  - Ensure the container host has been Hardened
[INFO] 1.3  - Ensure Docker is up to date
```


##### Clair
Clair - это приложение для анализа содержимого изображений и сообщения об уязвимостях, влияющих на содержимое. Это делается с помощью статического анализа, а не во время выполнения.
https://github.com/quay/clair


*Практические задания для текущего занятия*

1. Воспольщуйтесь линтером HADOLINT для анализа Dockerfile из предыдущих работ.
2. Воспользуйтесь приложением Dive для анлиза слоев вашего образа.
3. Представьте отчет о безопасности вашего приложения из Лабораторной 6.
4. Воспользуйтесь образом anchore/grype для анализа уязвимостей из приложения, используемого в задании 3.
5. Выполните сканирование уязвимостей произвольного образа при помощи docker scout, выполните рекомендации по повыщению надежности защиты, сканированного образа. Сделайте выводы по использованию данного инструменты с точки зрения автоматизации процесса сканирования образов.

Дополнительные источники
1. https://github.com/hadolint/hadolint
2. https://docs.docker.com/develop/scan-images/
3. https://swordfishsecurity.ru/blog/
4. obzor-utilit-bezopasnosti-docker
5. https://techrocks.ru/2020/04/27/docker-image-security/
6. https://github.com/docker/docker-bench-security
7. https://tech-geek.ru/docker-security/
8. https://itsecforu.ru/2022/09/09/%f0%9f%90%b3-%d0%bf%d0%be%d0%bf%d1%83%d0%bb%d1%8f%d1%80%d0%bd%d1%8b%d0%b5-%d0%be%d1%88%d0%b8%d0%b1%d0%ba%d0%b8-%d0%b2-%d0%ba%d0%be%d0%bd%d1%84%d0%b8%d0%b3%d1%83%d1%80%d0%b0%d1%86%d0%b8%d0%b8-%d0%ba/
9. https://docs.docker.com/scout/quickstart/
10. https://www.heyvaldemar.com/mastering-docker-scout-through-docker-desktop-gui-and-cli/
11. https://docs.docker.com/engine/reference/commandline/scout/