#### Docker Swarm 

Docker Swarm (или режим роя Docker) — это инструмент оркестрации контейнеров, упрощенный аналог Kubernetes. Он позволяет управлять несколькими контейнерами, развернутыми на нескольких хостах, на которых запущен Docker. 

Рассмотрим основные команды управления контейнирами в режиме аркесрации:

* инициализация кластера Docker Engines
* добавление узлов
* развертывание сервисов приложений
* команды управления 

Существуюет два типа машин в swarm кластере manager и worker, где manager управляет контейреми, оаспределяя по worker нодам, а worker узел на который распределяются контейнеры.


> Должны быть доступны следующие порты. В некоторых системах представленные нижу порты открыты по умолчанию.
* TCP-порт 2377 для управления кластером.
* Порт TCP и UDP 7946 для связи между узлами
* UDP-порт 4789 для оверлейного сетевого трафика


##### Инициализация кластера Swarm
Для создания кластера выберем систему, которая будет являться управляемым manager узлом и выполним команду

```bash
docker swarm init --advertise-addr <IP add>
```

В вываоде наблюдаем сгенерированную команду, позволяющую инициализировать worker узел
```bash
$ docker swarm init --advertise-addr 192.168.0.8
Swarm initialized: current node (jun7nndweicewg5i2cn42cwgo) is now a manager.

To add a worker to this swarm, run the following command:

docker swarm join --token SWMTKN-1-09nquu39t1znacthpl51omntawc958nre7q1fb2vllebz997cz-7l5yb7c3gvyrql5c2p281f2m4 192.168.0.8:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Перейдем на узел который хотим сделать worker и выполним команду
```bash
docker swarm join --token SWMTKN-1-09nquu39t1znacthpl51omntawc958nre7q1fb2vllebz997cz-7l5yb7c3gvyrql5c2p281f2m4 192.168.0.8:2377
```

После закрытия сессии терминала вывод терминала будет недоступным. По истечении времени нам может понадобиться возможность добавления worker узла. Выполним следующую команду для повторного получения токена. Вывод будет аналогичен предыдущей команде.

`$ docker swarm join-token worker`

Посмотрим состав узлов кластера 
```bash
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
jun7nndweicewg5i2cn42cwgo *   node1      Ready     Active         Leader           20.10.17
i1tkibuhbscv4s4e1jddm2wwl     node2      Ready     Active                          20.10.17
```

Для просмотра текущего статуса состояния кластера используем команду
```bash
$ docker info

Client:
 Context:    default
 Debug Mode: false
 Plugins:
  buildx: Docker Buildx (Docker Inc., v0.8.2)
  compose: Docker Compose (Docker Inc., v2.6.1)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
```

Удалим ноду из кластера 
Покинем кластер командой *docker swarm leave*
> данная команда выполняется на самой ноде.

Удалим ноду на упраляющем узле `docker swarm rm node_name`

> Статус работоспособности ноды можно поличить из команды *docker node ls*, а также *docker node inspect node_name*

##### Развертывание сервиса
> Все команды выполняются на управляющем узле

Создадим сервис
`$ docker service create --replicas 1 --name helloworld1 alpine ping vk.com`
Команда выше создает службу с названием helloworld1 при помощи флага *--name*. Флаг *--replicas* указывает желаемое состояние 1 работающего экземпляра. Аргументы *alpine ping vk.com* определяют службу как контейнер Alpine Linux, который выполняет команду *ping vk.com*.

Для просотра существующих сервисов выполним команду

```bash
$ docker service ls
ID             NAME          MODE         REPLICAS   IMAGE           PORTS
il0y472bvls1   helloworld    replicated   1/1        alpine:latest   
5bmknm0yz9g0   helloworld1   replicated   1/1        alpine:latest   
```
Для просмотра дополнительной информации о сервисе используем
`$docker service inspect helloworld1`

Вывод команды выше будет представлен в формате json. Для просмотра того же вывода в угобочитаемом выде воспользуемся аргументом *--pretty*.

```bash
$ docker service inspect --pretty helloworld1

ID:             5bmknm0yz9g0qdul3eeduqdsw
Name:           helloworld1
Service Mode:   Replicated
 Replicas:      1
Placement:
UpdateConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:         alpine:latest@sha256:ff6bdca1701f3a8a67e328815ff2346b0e4067d32ec36b7992c1fdc001dc8517
 Args:          ping vk.com 
 Init:          false
Resources:
Endpoint Mode:  vip
```

Для просмотра информации на каких узлах развернут сервис выполним команду.
```bash
$ docker service ps helloworld1
ID             NAME            IMAGE           NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
pi88ebxjknt0   helloworld1.1   alpine:latest   node2     Running         Running 11 minutes ago             
```
Детальную информацию о контретном контейнере можно получить уже знакомой командой `docker ps`

Удаление сервиса выполняется похожим образом как с контенерами при помощи команды *rm*.
```bash
$ docker service rm helloworld1
helloworld1
[node1] (local) root@192.168.0.13 ~
$ docker service ps helloworld1
no such service: helloworld1
```
##### Масштабирование сервиса и обновление версии

Одним из основных преимуществ docker swarm над классическим docker является масштабируемость приложений.
Запустим новый сервис *whoami* c одной репликой

`$ docker service create --replicas 1 --name whoami containous/whoami`

Увеличим количество реплик до 5
```bash
$ docker service scale whoami=5
whoami scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged 
```

Выведим информацию о распределении контейнеров по узлам
```bash
$ docker service ps whoami
ID             NAME       IMAGE                      NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
5atzgcmft80n   whoami.1   containous/whoami:latest   node1     Running         Running 13 minutes ago             
w61e4127psiq   whoami.2   containous/whoami:latest   node2     Running         Running 10 seconds ago             
z80j3zbdzrat   whoami.3   containous/whoami:latest   node2     Running         Running 10 seconds ago             
nwhrxtlvlqc3   whoami.4   containous/whoami:latest   node2     Running         Running 10 seconds ago             
h970fitvf913   whoami.5   containous/whoami:latest   node1     Running         Running 12 seconds ago 
```

> Для понижения количества реплик можно выполнить команду `docker service scale whoami=3`.
> При добавлении нового узала существующие контейнеры на него распределены не будут.

Для публикации порта при создании сервиса используется аргумент `-p`;
Удалим сервис и снова развенем.
```bash
$ docker service rm whoami
whoami

$docker service create --replicas 5 -p 80:80 --name whoami containous/whoami
```
> Стоит заметить, что после удаления сервиса контейнеры автоматиески останавливаются и удаляются
> При обновлении приложения развернутого выше видно, что запросы отравляемые от клиента отправляются на различные контейнеры и узлы

Зачастую возникает задача обновить образ контейнера, когда предыдущая версия приложения работает. Для ее решения существует команда *docker service update*.

`docker service update --image stefanscherer/whoami whoami`

Команда *docker service update* имеет множество дополнительных опций в случае если новыйобраз работает на другом порту изменим спецификацию, удалим старые порты и добавим новые.
`$docker service update --image stefanscherer/whoami --publish-rm published=80,target=80  --publish-add published=80,target=8080 whoami`

По умолчанию в настройке сервиса существуют следующие параметры:

```bash
UpdateConfig:
 Parallelism:   1
 Delay:         10s
```

Данные параменты мы можем изменять как при инициализации сервиса, так и при его изменении при помощи:
* аргумента --update-delay, который настраивает временную задержку между обновлениями. Можно указать время в минутах, секундах, часах. 
* аргумента --update-parallelism, который указывает количество контейнеров для одновременного обновления. Значение 1, контейнеры будут обновляться по одному, 0 все одновременно. Рекомендуется устанавливать значение меньше чем количество узлов в кластере.

Используемые образы для примеров:
* containous/whoami port 80
* stefanscherer/whoami port 8080

*Практические задания*
> Для выполнения практических заданий можете воспользоваться [play with docker](https://labs.play-with-docker.com). *nginxhello*

1. Выполните установку кластера с одной управляющей и двумя рабочими нодами.
2. Удалите рабочую нода из кластера и снова добавьте.
3. Создайте docker образ показывающий метаданные узла, включая ip адрес(Используйте любой язык программирования. Не забудьте открыть порт в создаваемом сервисе. EXPOSE).
4. Разверните сервис с 3 репликами (проверьте на какой ip вы попадаете).
5. Пересоберите образ указав дополнительную информацию о разработчике образа с вывод на главный интерфейс. Обновите приложение с использованием команды `docker service update`.


