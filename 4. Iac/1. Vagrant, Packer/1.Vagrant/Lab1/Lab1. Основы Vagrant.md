#### Основы Vagrant 

Vagrant — свободное и открытое программное обеспечение для создания и конфигурирования виртуальной среды разработки. Является обёрткой для программного обеспечения виртуализации, например VirtualBox, и средств управления конфигурациями, таких как Chef, Salt и Puppet.

##### Установка Vagrant и окружения
Установите следующие среды виртуализации Virtualbox

`sudo apt install virtualbox`

Установка Vagrant

https://www.vagrantup.com/downloads

Из коробки
`sudo apt install vagrant`

`vagrant --version`

##### Создание первой машины

Cоздадим каталог в домашней директории `stend`

Перейдем в него `cd stend`

Проинициализируем Vagrant 
`vagrant init`

В каталоге создался Vagrantfile
заменим строку

```
config.vm.box = "Base"
на
config.vm.box = "ubuntu/trusty64"
```

Выполним следующие команды
```bash
# Проверим статус 
vagrant status
# Проверим конфигурацию ssh
vagrant ssh-config 
# Пройдем внутрь Vm
vagrnt ssh
# удаляем машину 
vagrant destroy 
# Удалим каталог с содержимым
```

Замечание *если вы перейдете в иной каталог и попытесть повторить команды, то выйдет ошибка это происходит из-за того, что при инициализации vagrant init появляется файл .vagrant, содержащий всю информацию о текущим состоянии раазвертывания*

##### Использование KVM для работы с VM

Создадим каталог для нового проекта
Установим следующие пакеты для работы с KVM
```bash
sudo apt install -y qemu qemu-kvm libvirt-daemon libvirt-daemon-system libvirt-clients bridge-utils virt-manager
sudo apt install -y libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev ruby-libvirt ebtables dnsmasq-base
```
Проверим, что демон работает
`sudo systemctl status libvirtd`
Установим плагин libvirt для vagrtant

```bash
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-mutate
```
Проверим, что плагин установлен
`vagrant plugin list`

Перезагрузим систему для того чтобы система увидела libvirtd и установленный плагин.


Проиницилизируем вагранфайл следующим образом
`vagrant init generic/centos8`
Список всех доступных боксов имеется на официальном сайте vagrant в разделе необходимого провайдера.

Выполним старт Vagrant с провайдером libvirt
`vagrant up --provider=libvirt`

Проверьте создание VM в KVM
Vagrant поддерживает несколько провайдеров для обеспечения удобства и гибкости в работе:
* Virtualbox;
* Docker;
* VMware;
* Hyper-v;
* KVM.

Просмотрим список всех боксов на локальной машине
`vagrant box list`

Обновить боксы можно при помощи команды 
`vagrant box update`
Вагрант самостоятельно приведет боксы к актуальному состоянию.

Провалимся внутрь бокса и установим пароль
`vagrant ssh `

Становимся администратором
`sudo su`
Меняем пароль
`passwd`
Выйдем из машины командой `exit`
Выключим машину, после выполнения каждой из команд ниже используйте `vagrant status`
`vagrant halt`
Для возобновления работы машины используйте
`vagrant reload`
Для приостановки и сохранения текущего состояния Vm, выполним команду
`vagrant suspend`
Для возобновления работы воспользумся командой
`vagrant resume`
Удалим созданную машину.
`vagrant destroy`

Рассмотрим подробнее конфигурирования Vm и их создание, для этого познакомимся с Vagrantfile.

##### Vagrantfile

В созданном Vagrantfile уже существуют подсказки к настройке сред, рассмотрим пример создания нескольких машин, для понимая сткуктуры файла.

```Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = 1024  
    lv.cpus = 1
  end
  config.vm.define "prod" do |prod|
    prod.vm.box = "generic/centos8"
  end

  config.vm.define "test" do |test|
    test.vm.box = "generic/centos8"
  end
end
```

В данной конфигурации мы создали две машины, конфигурации которых соответствуют друг другу, одна, будет предназначена для продакшена, другая для тестов.

Для попадания внутрь конкретной машины используйте следующий пример `vagrant ssh prod`.

Представим ситуацию, что для Vm prod нам понадобилось больше памяти, соответственно для данной машины будем использовать свои настройки провайдера как показано в скрипте
```Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = "512"
    lv.cpus = "1"
  end

  config.vm.define "prod" do |prod|
    prod.vm.box = "generic/centos8"
    prod.vm.provider "libvirt" do |lv|
      lv.memory = "520"
      lv.cpus = "1"
    end
  end

  config.vm.define "test" do |test|
    test.vm.box = "generic/centos8"
  end
end
```
Проверим командами внутри машин `free -h` и `nproc`

Примеры конфигурирования будем рассматривать на одной машине (для экономия ресурсов и времени). Разница будет в том, что общие настройки будут с ключом config, а персональные настройки для машин соответственной с ключами prod и test.

##### Простое конфигурирование Vagrantfile
Установим hostname и статический IP адресс
```Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.define "test" do |test|
    test.vm.box = "generic/centos8"
    test.vm.network "private_network", ip: "192.168.100.2"
    test.vm.hostname = "test"
    test.vm.provider "libvirt" do |lv|
    # Customize the amount of memory on the VM:
    lv.memory = "512"
    lv.cpus = "1"
    end
  end
end
```

Проверим настройки, перейдя внутрь машины `vagrant ssh test` и выполним команды `ip addr && hostname`.
Данная конфигурация позволит обращаться к машине по hostname. 

Для того, чтобы настроить виртуальную машину и автоматически во время создания установить нужные пакеты. Необходимо в конфигурации указать следующую строку.
`config.vm.provision "shell", inline: $script`
И в в самом начале документа укажем секцию SCRIPT, которая позволяет выполнить команды на машине.
```
ls -la
mkdir test_dir
ls -la
SCRIPT
```
Полный Vagrantfile
```Vagrantfile
$script = <<-SCRIPT
ls -la
mkdir test_dir
ls -la
SCRIPT
Vagrant.configure("2") do |config|
  config.vm.define "test" do |test|
    test.vm.box = "generic/centos8"
    test.vm.hostname = "test"
    test.vm.provision "shell", inline: $script
    test.vm.provider "libvirt" do |lv|
    # Customize the amount of memory on the VM:
    lv.memory = "512"
    lv.cpus = "1"
    end
  end
end
```
При создании машины вы можите видеть как отрабатывает секция `inline script`

Изменим Vagrantfile таким образом, чтобы все shell команды, находились в отдельном файле, данный подход позволяет исключить громоздкие записи в одном файле.
Удалим блок скрипт и заменим строку
```bash
#test.vm.provision "shell", inline: $script
на
test.vm.provision "shell", path: "script.sh"
```

Обеспечение ВМ нужной конфигурации может осуществляться нетолько на уровне bash скриптов, но и с инспользованием следующих технологий:
* Ansible;
* Chef;
* Puppet;
* Salt;
* Podman;
* Docker.
Добавим есколько дисков в конфигурацию vagrantfile

Добавим диск к имеющейся конфигурации.

`lv.storage :file, :size => '10G', primary: true`
Для проверки воспользуемся следующими командами:
`sudo fdisk -l`
`lsblk`

*Практические задания*
1. Установите провайдер libvirt и произведите его конфигурацию.
2. Настройте Vagrantfile и запустите c ubuntu версии 16.04.
3. Установите веб-сервер apache и проверьте его работоспособность, используя скрипты или отдельно подключить в Vagrantfile.
4. Убедитесь, что службы работает исправно, протестируйте правильное ее поведение утилитами wget и curl внутри ВМ.
5. Пробросьте порты apache на внешний хост и проверьте работоспособность веб-сервера под хостовой машиной. 
6. Используя внешний скрипт установите стандартные утилиты для работы с ОС Linux vim, htop, nettools;
7. Разверните 2 VM предоставив 256MB оперативной памяти, и 1 CPU.

*Дополнительные источники*
1. https://www.vagrantup.com/
2. https://help.ubuntu.ru/wiki/vagrant