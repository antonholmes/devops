#### Расширенная функциональность Vagrant

Предыдущая практика позволила познакомиться с основными функция Vagrant.
Данное практиское занятие позволит углубиться в данный инструмент.

##### Дополнительные команды CLI Vagrant

Из первой практики очевидно, что формат использования образов в Vagrant .box.
Научимся ими управлять.

Просмотр сприска образов
```bash
xub1@xub1:~/project/devops$ vagrant box list
generic/centos8    (libvirt, 3.5.4)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)
```

Как и в docker, так и у боксов Vagrant существуют свои версии.
Проверим доступность новой версии в боксе для провайдера libvirt. Стоит заметить, что данная команда отработает в случа, если в текущей директории существет Vagrantfile. 

```bash
xub1@xub1:~/project/devops/4. Iac/1.Vagrant, Packer/1.Vagrant/Lab1$ vagrant box outdated
Checking if box 'generic/centos8' version '3.5.4' is up to date...
A newer version of the box 'generic/centos8' for provider 'libvirt' is
available! You currently have version '3.5.4'. The latest is version
'3.6.6'. Run `vagrant box update` to update.
```

В выводе команды, просят обновить бокс командой vagrant box update.

Данная команда, не обновляет старый образ, а скачивает новый.
```bash
xub1@xub1:~/project/devops/4. Iac/1.Vagrant, Packer/1.Vagrant/Lab1$  vagrant box list
generic/centos8    (libvirt, 3.5.4)
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)
```
Удалить старые образы возможно командой 

xub1@xub1:~/project/devops/4. Iac/1.Vagrant, Packer/1.Vagrant/Lab1$ vagrant box prune
```bash
The following boxes will be kept...
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)

Checking for older boxes...
Box 'generic/centos8' (v3.5.4) with provider 'libvirt' appears
to still be in use by at least one Vagrant environment. Removing
the box could corrupt the environment. We recommend destroying
these environments first:

test (ID: 4cc3061358ad4ae2815e04302dc2aab0)

Are you sure you want to remove this box? [y/N] y
Removing box 'generic/centos8' (v3.5.4) with provider 'libvirt'...
```

Все боксы в Linux и MacOS храняться по следующему пути
```bash
xub1@xub1:~/.vagrant.d/boxes$ ls
generic-VAGRANTSLASH-centos8  generic-VAGRANTSLASH-ubuntu1604  ubuntu-VAGRANTSLASH-trusty64
```
В Windows C:/Users/USERNAME/.vagrant.d/boxes./

Просмотреть, сколько занимает памяти бокс можно перейдя по пути и выполнив команду `ls -lh`.
```bash
xub1@xub1:~/.vagrant.d/boxes/ubuntu-VAGRANTSLASH-trusty64/20190514.0.0/virtualbox$ ls -lh
итого 429M
-rw------- 1 xub1 xub1 429M дек 19 07:11 box-disk1.vmdk
-rw------- 1 xub1 xub1  11K дек 19 07:11 box.ovf
-rw-rw-r-- 1 xub1 xub1   25 дек 19 07:11 metadata.json
-rw-r--r-- 1 xub1 xub1  505 дек 19 07:11 Vagrantfile
```
Либо проверить размер каталога с его содержимым командай `du -sh`
```bash
xub1@xub1:~/.vagrant.d/boxes$ du -sh ubuntu-VAGRANTSLASH-trusty64/
429M    ubuntu-VAGRANTSLASH-trusty64/
```

Для удаления бокса используйте команду remove
```vagrant box remove ubuntu/trusty64```
```bash
xub1@xub1:~/project/devops$ vagrant box list
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)
xub1@xub1:~/project/devops$ vagrant box remove ubuntu/trusty64
Removing box 'ubuntu/trusty64' (v20190514.0.0) with provider 'virtualbox'...
xub1@xub1:~/project/devops$ vagrant box list
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
```
На предыдущей практике мы обеспечивали надлежащие состояние машины при помоще скрипта, что если все что-то пошло не так и не выполнились инструкции. Для этого существует команда `vagrant provision`, которая при работающей ВМ, позволяет пересобрать конфигурацию машины с учетом исправлений. Стоит учесть, что первоначально выполненные команды при развертывании определяют текущее состояние машины.

Как и docker Vagrant в основном предназначен для быстрого создания тестовых стендов. Соответственно для разработчиков необходимо предоставить полностью настроенную среду. Мы уже умеем устанавливать и настраивать необходимое окружение, осталось научиться монтировать каталоги, для того чтобы разработчик умел локально разрабатывать и сразу просматривать готовый результат на стенде. Для этого существует возможность пробразывать каталог внутрь ВМ.

`test.vm.synced_folder "./html", "/var/www/html"`
Если извне постучаться на порт 80 nginx, то мы увидим полностью новую страничку html. Более подробно о настроке веб сервера Nginx пройдем на последующих практиках.

##### Использование переменные и создание нескольких машин

В теоретических сведениях предыдущей практики вы видели пример с развертыванием нескольких машин в одном Vagrantfile.
Рассмотрим следующий пример, предположим мы хотим развернуть несколько инстенсов базы данных в случае утери одного данные бы надежно были сохранены.
```Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = 1024  
    lv.cpus = 1
  end
  (1..2).each do |i|   #
  config.vm.define "prod#{i}" do |prod| #
  prod.vm.box = "generic/ubuntu1610"
  prod.vm.hostname = "node#{i}"
  end
 end
end
```


В данном примере используем следующую конструкцию, представляющую собой цикл `(1..2).each do |i|`, который позволяет развертывать виртуальные машины друг за другом. Значения 1 и 2 будут попадать в переменную i, каждое в своей итерации.
Поэтому насвание виртуальных машин будет prod1 и prod2, а имя хоста будет node1 и node2.

Вышеприведенный пример, использует переменную i в качестве идентификатора каждой из машин, для удобства настройки Vagrantfile используются переменые определяемые в самом начале манифеста. Данный подход, очень удопен при настройке множества виртуальных машин.

```Vagrantfile
BOX_IMAGE = "generic/ubuntu1610"
NODE_COUNT = 2

Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = 1024  
    lv.cpus = 1
  end
  (1..NODE_COUNT).each do |i|
  config.vm.define "prod#{i}" do |prod|
  prod.vm.box = BOX_IMAGE
  prod.vm.hostname = "#{NODE_COUNT}#{i}"
  end
 end
end
```

В Vagrant существует возможность создавать свои собственные боксы.

Перым способом является использование уже существующего образа, и построение на его основе нового бокса. Для этого мы можете изменить внутреннюю, конфигурацию(установить или удалить необходимые пакеты.)

Находясь в каталоге с Vagrantfile, выполните команду `vagrant halt`(для остановки бокса).

Следущим этапом необходимо сделать снапшот состояния виртуальной машины.
`vagrant package --output dockerubuntu14.box `

Для создания бокса из снапшота выполните команду 
`vagrant box add dockerubuntu14 dockerubuntu14.box`

где dockerubuntu14, будет своего рода названием для боксов одного типа виртуальных машин.

При просмотре списка vagrant боксов, вы можеет заметить, что его версия будет '0', для задания собственной версии добавьте в Vagrantfile следующую строку.

`config.vm.box_version = "20211020.0.0"`


Cуществует несколько способов создания своих боксов на основе iso образа.

Первый включает в себя установку операционной системы в гипервизоре например в Virtualbox либо используя автоматизацию при помощи Packer.
И последующее выполнение команды 
`vagrant package --output alpine-test.box --base alpine`
где в параметре `--base` указывается название вышей виртуальной машины. С последующим созданием образа
`vagrant box add alpine-test alpine-test.box`

1. Создайте новую папку для своего проекта Vagrant и перейдите в нее в командной строке или терминале.

2. Используйте команду `vagrant init` для инициализации нового проекта Vagrant в текущей папке. Это создаст файл с названием `Vagrantfile`, который будет содержать настройки вашего проекта.

3. Откройте файл `Vagrantfile` в текстовом редакторе и внесите следующие изменения:

   - Замените значение `config.vm.box` на имя вашего Vagrant box (например, "my-box").
   - Установите значение `config.vm.box_url` на пустую строку. Либо `ENV['VAGRANT_SERVER_URL'] = ''` перед строкой настройки виртуальной мышины vagrant  `Vagrant.configure("2") do |config|`
   - Добавьте следующий код, чтобы настроить виртуальную машину:

```
     config.vm.define "my-box" do |machine|
       machine.vm.box = "my-box"
       machine.vm.provider "virtualbox" do |vb|
         vb.memory = "1024"
         vb.cpus = "2"
       end
     end
```     

   - Замените значение `vb.memory` и `vb.cpus` на желаемые значения для вашей виртуальной машины.

4. Создайте новую папку с названием `iso` внутри вашего проекта Vagrant и скопируйте в нее загруженный ISO образ.

5. Вернитесь в командную строку или терминал и выполните следующую команду, чтобы добавить ISO образ в ваш Vagrant box:

   
```bash
   vagrant box add my-box path/to/iso/file
```   

   Замените `my-box` на имя вашего Vagrant box и `path/to/iso/file` на путь к загруженному ISO образу.

8. Запустите виртуальную машину, используя команду `vagrant up`. Vagrant автоматически создаст новую виртуальную машину на основе ISO образа и настроит ее в соответствии с вашим файлом `Vagrantfile`.


*Практические задания*
1. Проверьте доступны ли новые боксы.
2. Удалите все боксы занимающие больше 700Mb метса на диске.
3. Создайте общий каталог для гостевой машины и хостовой, пробростьте произвольный index.html и убедитесь, что при его изменении, соджержимое изменяется на сервере nginx.
4. Разверните две машины, используя переменные установите на одну СУБД PostgeSql, на другую apache.
5. Развертите две одинаковые машины, установив на которые СУБД MongoDB, используя переменные. 
Практические задания считаются выполеными, если вы успешно проверили работоспособность установленного програамного обеспечения, для этого рекомендуется и этот процесс автоматизировать добавив необходимые конструкции в инструкцию в скрипт.
6. Создайте собственный бокс на основе существующего образа и добавьте ему версию 0.0.1.
7. Создайте бокс на основе образа alpine linux/

Литература
https://www.vagrantup.com/docs/cli/box#box-prune
https://help.ubuntu.ru/wiki/vagrant
http://alexconst.net/2016/01/28/vagrant/
https://github.com/vagrant-libvirt/vagrant-libvirt
https://ru.linux-console.net/?p=21901