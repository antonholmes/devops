packer {
  required_plugins {
    docker = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

source "virtualbox-iso" "basic-example" {
  guest_os_type    = "Ubuntu_64"
  iso_url          = "http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/mini.iso"
  iso_checksum     = "md5:8388f7232b400bdc80279668847f90da"
  ssh_username     = "packer"
  ssh_password     = "packer"
  ssh_wait_timeout = "1180s"
  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
}

build {
  sources = ["sources.virtualbox-iso.basic-example"]
}


