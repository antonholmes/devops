#!/bin/bash
#curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh

mkdir docker
cd docker 
sudo apt install git 
sudo apt install curl
sudo apt install vim
# https://gitlab.com/antonholmes/devops/-/blob/main/3.%20Docker/docker_install.sh
# touch docker_install.sh
# chmod 777 docker_install.sh 
# ./docker_install.sh

sudo apt update -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce -y
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker

sudo curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version

sudo docker swarm init
# sudo docker run -itd -p 81:80 nginx:alpine
# ip a