#### Terraform


##### Terraform install
`sudo apt install terraform`

```bash
xub1@xub1:~$ terraform --version
Terraform v1.1.0
on linux_amd64

Your version of Terraform is out of date! The latest version
is 1.1.2. You can update by downloading from https://www.terraform.io/downloads.html
```

https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started

##### Быстрый старт на примере nginx и провайдера docker

Создадим новый каталог 
`mkdir learn-terraform-docker-container`
Перейдем в него 
`cd learn-terraform-docker-container`
Создадим файл main.tf
`touch main.tf`
Отредактируем, добавив следующее содержимое

```terraform
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "tutorial"
  ports {
    internal = 80
    external = 8000
  }
}
```

Проинициализируем проект, который загружает плагин Docker.
`terraform init`
Применим конфигурацию nginx. Введите `yes`
`terraform apply`

Проверим самочувствие котейнера командой `docker ps` и перейдем localhost:8000
```bash
xub1@xub1:~/project/devops/learn-terraform-docker-container$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                  NAMES
3f65fa119f2b   ea335eea17ab   "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes   0.0.0.0:8000->80/tcp   tutorial
```
Проверим ответ сервера при помощи команды `curl localhost:8000`

Завершим работу ресурсов, текущего проекта Terraform. Введите `yes`
`terraform destroy`

##### Простой пример с провайдером libvirt
Создадим новый каталог `lib` и перейдем в него
`mkdir lib && cd lib`
Создадим файл `main.tf` с следующим содержимым
```terraform
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  # Configuration options
}
```
Установим провайдер libvirt для Terraform и проинициализируем проект.
`terraform init`

Изменим файл `main.tf`, так чтобы был определен локальный провайдер libvirt и укажем на развертывание тестового ресурса.

Выполним команды:
```bash
terraform init
# Следующая команда определяет какие действия будут выполняться terraform
terraform plan
terraform apply
# Заметим, что terraform создал пустой ресур без ОС.
terraform destroy
```
##### Управление ресурсами
https://www.terraform.io/language
