#### Установка Minikube


##### Установка kubectl
Инструмент командной строки Kubernetes kubectl позволяет запускать команды для кластеров Kubernetes. Вы можете использовать kubectl для развертывания приложений, проверки и управления ресурсов кластера, а также для просмотра логов. 

Установка [kubectl](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/#%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-kubectl-%D0%B2-linux)
```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```
##### Установка Minikube
Если у вас ещё не установлен гипервизор, установите один из них:

• [KVM](https://www.linux-kvm.org/page/Main_Page), который также использует QEMU

• [VirtualBox](https://www.virtualbox.org/wiki/Downloads)


Установка Minikube с помощью прямой ссылки

Вы также можете загрузить двоичный файл и использовать его вместо установки пакета:
```bash
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
```

Чтобы исполняемый файл Minikube был доступен из любой директории выполните следующие команды:
```bash
sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
```
Чтобы убедиться в том, что гипервизор и Minikube были установлены корректно, выполните следующую команду, которая запускает локальный кластер Kubernetes:
```bash
minikube start --vm-driver=<driver_name>
```
где driver_name - название драйвера гипервизора. [Список драйверов](https://minikube.sigs.k8s.io/docs/drivers/).

```bash
minikube start --driver=virtualbox
```
Убедимся что minikube запущен
```bash
openstack@openstack-Standard:~$ minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Если вы ваполнили старт minikube без ключа, `--vm-driver`, вы моджете его пересоздать, используя следующий набор команд:

```bash
minikube stop
minikube delete
minikube start --driver=virtualbox
```

##### Настройка автодополнения команд

Скрипт дополнения ввода kubectl для Bash может быть сгенерирован с помощью команды `kubectl completion bash`. Подключение скрипта дополнения ввода в вашу оболочку включает поддержку автозаполнения ввода для kubectl.
Однако скрипт дополнения ввода зависит от `bash-completion`, поэтому вам нужно сначала установить этот пакет (вы можете выполнить команду `type _init_completion`, чтобы проверить, установлен ли у вас уже bash-completion).

bash-completion можно установить через многие менеджеры пакетов. Вы можете установить его с помощью `apt-get install bash-completion` или `yum install bash-completion` и т.д.

Приведенные выше команды создадут файл /usr/share/bash-completion/bash_completion, который является основным скриптом bash-completion. В зависимости от используемого менеджера пакетов, вы можете подключить этот файл в файле ~/.bashrc.

Чтобы убедиться, что этот скрипт выполняется, перезагрузите оболочку и выполните команду `type _init_completion`. Если команда отработала успешно, установка сделана правильно, в противном случае добавьте следующее содержимое в файл ~/.bashrc:

##### Краткое знакомство с кластером

Давайте создадим развёртывание (Deployment) в Kubernetes, используя существующий образ echoserver, представляющий простой HTTP-сервер, и сделаем его доступным на порту 8080 с помощью --port.

```bash
kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.10
```
Вывод:
```bash
    service/hello-minikube exposed
```

Под (Pod) hello-minikube теперь запущен, но нужно подождать, пока он начнёт функционировать, прежде чем обращаться к нему.

Проверьте, что под работает:
```bash
    kubectl get pod
```
Если в столбце вывода STATUS выводится ContainerCreating, значит под все еще создается:
```bash
    NAME                              READY     STATUS              RESTARTS   AGE
    hello-minikube-3383150820-vctvh   0/1       ContainerCreating   0          3s
```

Если в столбце STATUS указано Running, то под теперь в рабочем состоянии:
```bash
    NAME                              READY     STATUS    RESTARTS   AGE
    hello-minikube-3383150820-vctvh   1/1       Running   0          13s
```
Чтобы получить доступ к объекту Deployment hello-minikube извне, создайте объект сервиса (Service):
```bash
kubectl expose deployment hello-minikube --type=NodePort --port=8080
```

вывод
```bash
    service/hello-minikube exposed
```

Узнайте URL-адрес открытого (exposed) сервиса, чтобы просмотреть подробные сведения о сервисе:
```bash
    minikube service hello-minikube --url
```
Чтобы ознакомиться с подробной информацией о локальном кластере, скопируйте и откройте полученный из вывода команды на предыдущем шаге URL-адрес в браузере.

Вывод будет примерно следующим: 
```bash
Hostname: hello-minikube-5d9b964bfb-c7kzm

Pod Information:
	-no pod information available-

Server values:
	server_version=nginx: 1.13.3 - lua: 10008

Request Information:
	client_address=172.17.0.1
	method=GET
	real path=/
	query=
	request_version=1.1
	request_scheme=http
	request_uri=http://192.168.99.100:8080/

Request Headers:
	accept=text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
	accept-encoding=gzip, deflate
	accept-language=ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
	connection=keep-alive
	host=192.168.99.100:31773
	upgrade-insecure-requests=1
	user-agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0

Request Body:
	-no body in reques
```
Удалите сервис hello-minikube:
```bash
    kubectl delete services hello-minikube
```

Удалите развёртывание hello-minikube:
```bash
    kubectl delete deployment hello-minikube
```
Задание: 
1. Повторите практические примеры
2. Ознакомьтесь с `minikube dashboard`, выполнив данную команду в консоли;
3. Подробно рассмотрите команду `minikube --help`

Задание на самостоятельную подготовку
1. Изучите возможности kubespray
2. Ознакомьтесь с основными компонентами [k8s](https://kubernetes.io/ru/docs/concepts/overview/what-is-kubernetes/) 
3. Изучите возможность использования minikube без драйвера виртуализации, вместо указания драйвера используйте none.

Команда переключения между кластерами
`kubectl config use-context` 