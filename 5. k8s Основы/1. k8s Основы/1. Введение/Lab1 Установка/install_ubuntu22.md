
sudo hostnamectl set-hostname node-1

sudo vim /etc/hosts

192.168.0.6 k8s
192.168.0.8 node-1
192.168.0.9 node-2

sudo kubeadm init \
  --pod-network-cidr=10.244.0.0/16 \
  --cri-socket /run/containerd/containerd.sock \
  --upload-certs \
  --control-plane-endpoint=k8s

-------------------

kubeadm init --apiserver-advertise-address=<публичный IP-адрес> --pod-network-cidr=10.244.0.0/16

--image-repository docker.io 
--kubernetes-version=1.20.0


  kubeadm join k8s:6443 --token 0j89tq.fg17phxe2edcncu6 \
	--discovery-token-ca-cert-hash sha256:f144f57e0d8f53bdcc448e9f97196d696420eee996ce95e60916e1d515687870 \
	--control-plane --certificate-key d2b20cf63ce30ddaa01356a1d0b3f892bbc1cad57368a2417db728bb1aa8e6e9

Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use
"kubeadm init phase upload-certs --upload-certs" to reload certs afterward.

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join k8s:6443 --token 0j89tq.fg17phxe2edcncu6 \
	--discovery-token-ca-cert-hash sha256:f144f57e0d8f53bdcc448e9f97196d696420eee996ce95e60916e1d515687870 


Проверим
```
k8s@k8s:~$ kubectl cluster-info
Kubernetes control plane is running at https://k8s:6443
CoreDNS is running at https://k8s:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Устанавливаем сеть

wget https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml

Проверяем состояние кластера
```bash
k8s@k8s:~$ kubectl get nodes -o wide
NAME   STATUS   ROLES           AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
k8s    Ready    control-plane   5m12s   v1.28.2   192.168.0.6   <none>        Ubuntu 22.04.3 LTS   5.15.0-91-generic   containerd://1.6.25
```

sudo kubectl --kubeconfig=/home/k8s/.kube/config apply -f kube-flannel.yml



Представим если вы хотите перенести мастер на другую систему без сохранения состояния контейнеров(скопировать конфигурацию кластера, а на нем уже был запущен k8s)

выход из инициализации кластера 
sudo kubeadm reset

rm  $HOME/.kube/config

(Выполните инициализацию заново)


journalctl -xeu kubelet > error.txt
k8s@k8s:~$ sudo systemctl restart kubelet
k8s@k8s:~$ kubectl get events


Если постоянно pending
kubectl taint nodes --all node-role.kubernetes.io/control-plane-

https://computingforgeeks.com/install-kubernetes-cluster-ubuntu-jammy/


для добавления новой ноды используем 
sudo kubeadm reset

добавляем токен
для его генерации если забыли выполняем команду

kubeadm token create --print-join-command
вывод

kubeadm join 192.168.0.9:6443 --token 4kqni2.0pua9qb1kyvchr5i --discovery-token-ca-cert-hash sha256:53e9480977c65abb069b95dff7a2b8b612c5f34ba35276e30711fe9353aadfc8 

команду выполняем на worker ноде

перезагружаем сервисы
sudo systemctl restart containerd
sudo systemctl restart kubelet