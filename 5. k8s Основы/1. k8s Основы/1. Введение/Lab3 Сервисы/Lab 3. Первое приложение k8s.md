### Первое приложение k8s  

#### Развернем простое приложение на go

[+] скачиваем готовое приложение
[+] cоберем
[+] поместим в реестр
[+] готовим первый deployment.yml
[+] Проверим, что все работает
[+] Рассотрим из чего состоит deployment
[+] готовим сервис service.yml

`git clone repo_name`

Собираем образ
`docker build -t  demo_hello .`
Убедимся, что образ создался
```bash
minikube@minikube:~/test-book/demo/hello$ docker images
REPOSITORY                    TAG           IMAGE ID       CREATED              SIZE
<none>                        <none>        7dcda7c300b9   About a minute ago   336MB
demo_hello                    latest        346265102505   About a minute ago   6.07MB
```

Запустим котейнер и убедимся, что все работает 
`docker run --name hello  -p 9999:8888 demo_hello`

localhost:9999/ или 127.0.0.1:9999/

Повторим операцаю размещения образа в реестре.
Для того чтобы разместить свой локальный образ в реестре, вам нужно назвать его в таком формате: `YOUR_DOCKER_ID/myhello .`
Для создания этого имени не обязательно пересобирать весь образ — достаточно выполнить следующую команду:

`docker image tag demo_hello 333355/demo_hello`

Это делается для того, чтобы при загрузке образа в реестр Docker знал, в какой учетной записи его нужно хранить.
Загрузите свой образ в Docker Hub с помощью команды:
`docker image push 333355/myhello`

Создадим манифест для кластера deploy_demo_hello.yml с следующим содержимым
```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello
  labels:
    app: hello
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello
  template:
    metadata:
      labels:
        app: hello
    spec:
      containers:
      - name: hello
        image: 333355/demo_hello
        ports:
        - containerPort: 8888
```

Убелимся, что в кластере все пусто
`kubectl get pod`

Применим манифест
`kubectl apply -f deploy_demo_hello.yml`
Убедимся, что все работает
```bash
minikube@minikube:~/test-book/demo/hello/yml$ k get pod
NAME                     READY   STATUS    RESTARTS   AGE
hello-7548c9dfdb-prx5z   1/1     Running   0          26m
minikube@minikube:~/test-book/demo/hello/yml$ k get deployment
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
hello   1/1     1            1           52m
```

Проверим работоспособность командой
`kubectl port-forward hello-7548c9dfdb-prx5z 9999:8888`
Перейдем на localhost:9999 и убедимся, что все работает. д

Разберем более подробно содержание манифеста.


Проброс порта из pod не является лучшей практикой, а тем более не подходим для производственных сред. Для коммутации между множествами pod существует сервис, под названием service.
ОПИШИ ЧТО ТАКОЕ СЕРВИС

Сервис типа service

```yml
apiVersion: v1
kind: Service
metadata:
  name: hello
  labels:
    app: hello
spec:
  selector:
    app: hello
  ports:
    - protocol: TCP
      port: 8888
      targetPort: 8888
  type: LoadBalancer
```

```bash
minikube@minikube:~/test-book/demo/hello/yml$ kubectl apply -f service_demo_hello.yml 
service/hello created
```
Просмотрим все сервисы в кластере
<pre><font color="#4CE64C"><b>minikube@minikube</b></font>:<font color="#295FCC"><b>~/test-book/demo/hello/yml</b></font>$
k get service
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
demo         LoadBalancer   10.101.67.174    &lt;pending&gt;     8888:30471/TCP   10m
hello        LoadBalancer   10.110.225.122   &lt;pending&gt;     8888:31266/TCP   62m
kubernetes   ClusterIP      10.96.0.1        &lt;none&gt;        443/TCP          162m
</pre>

Пробросим порты и убедимся, что все работает localhost:9999
`kubectl port-forward service/hello 9999:8888`

#### Пример развертывания Moodle в кластере

Практические задания


