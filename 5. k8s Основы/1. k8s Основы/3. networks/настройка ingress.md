### Простая настройка ingress

NGINX Ingress

[] minikube addons enable ingress
[] возьмем stateless приложение и пробросим через ингресс 

minikube addons enable ingress

vim /etc/hosts add hostname

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: learngit
spec:
  tls:
  - hosts:
    - cafe.example.com
    secretName: learngit
  rules:
  - host: cafe.example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: learngit
            port:
              number: 80
      - path: /coffee
        pathType: Prefix
        backend:
          service:
            name: coffee-svc
            port:
              number: 80

TRAEFIK