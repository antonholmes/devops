#### Подключение к кластеру из локальной сети 

- Устанавливаем клиен в виде `kubectl`
- Переходим на масте ноду и копируем файл на `.kube/config`
- Добавляем переменную окружения `export KUBECONFIG=/home/minikube/config.yaml`

Выполняем подключение 
```bash
minikube@minikube:~$ kubectl get pod
NAME                     READY   STATUS    RESTARTS      AGE
hello-6747c5979f-2vj24   1/1     Running   2 (39m ago)   40h
hello-6747c5979f-8d4kl   1/1     Running   2 (39m ago)   40h
hello-6747c5979f-fs98h   1/1     Running   3 (39m ago)   4d16h
hello-6747c5979f-nvl8v   1/1     Running   2 (39m ago)   40h
hello-6747c5979f-svbpd   1/1     Running   2 (39m ago)   40h
```

https://www.shellhacks.com/minikube-start-with-more-memory-cpus/

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
kubectl get events
kubectl config view