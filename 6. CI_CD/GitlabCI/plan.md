Gitlab CI/CD


1.1. Погружение в CI/CD 
2.1. Принципы работы CI и CD 
2.2. Быстрый старт с Gitlab 
3.1. Инструменты для построения CI/CD 1 
3.1. Инструменты для построения CI/CD 2 
4.1. Из чего состоит Gitlab и как его установить 
5.1. Рекомендации по работе с Gitlab 
5.2. Первый проект в Gitlab на практике 
6.1. Задачи и возможности gitlab runner, его установка 
7.1. Основные понятия при написании файла конфигурации 
7.2. Пишем простой файл .gitlab-ci.yml: сборка, тесты, развертывание 1 
7.2. Пишем простой файл .gitlab-ci.yml: сборка, тесты, развертывание 2 
7.3. Deploy приложения в кластер Kubernetes 
8.1. Продвинутые приемы работы с Gitlab CI/CD pipeline 
9.1. Добавление в пайплайн возможности Rollback 
9.2. Работа с динамическими окружениями 
10.1. Интеграция Gitlab CI/CD и кластера Kubernetes 
11.1. Push и Pull модели работы; применяем ArgoCD на практике 
12.1. Скрытие переменных CI/CD, интеграция с Vault 1 
12.1. Скрытие переменных CI/CD, интеграция с Vault 2 
12.2. Статический анализ кода на безопасность (SAST) 1 
12.2. Статический анализ кода на безопасность (SAST) 2 