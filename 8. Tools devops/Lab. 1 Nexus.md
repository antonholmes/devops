#### Nexus
Sonatype Nexus – интегрированная платформа, с помощью которой разработчики могут проксировать, хранить и управлять зависимостями Java (Maven), образами Docker, Python, Ruby, NPM, Bower, RPM-пакетами, gitlfs, Apt, Go, Nuget, а также распространять свое программное обеспечение. 

##### Установка Nexus 
```docker-compose
version: "3"

services:
  nexus3:
    image: sonatype/nexus3
    volumes:
    - ./nexus-data:/nexus-data
    ports:
    - 8081:8081
    - 8101:8101
    - 8102:8102

```
Выполним `docker-compose up -d`
Убедимся, что Nexus установлен выполним `docker-compose logs`
Установим права на каталог `sudo chmod 777 /nexus-data`
Подключимся к docker-контейнеру с оболочкой bash:
docker exec -it -u 0 nexus bash
Установим пакет pinentry, для того, чтобы можно было дальше сгенерировать ключи для apt репозитори
yum -y install pinentry
##### Работа с приватрым репозиторием docker

Перейдите на другой подконтрольный вами хост и подключитесь к Nexus
```bash
docker login -u user -p 12345 192.168.0.150:8101
docker pull mongo:latest
docker tag mongo:latest 192.168.0.150:8101/mongo:latest
docker push 192.168.0.150:8101/mongo:latest
# Конфигурационный файл для входа в реестр Docker находятся
cat ~/.docker/config.json
```

Чтобы запустить команду docker login в неинтерактивном режиме, вы можете установить флаг --password-stdin для предоставления пароля через STDIN. Использование STDIN предотвращает попадание пароля в историю оболочки или файлы журнала.

```bash
cat ~/my_password.txt | docker login --username foo --password-stdin

# or

docker login --username foo --password-stdin < ~/my_password
```

Предыдущие команды можно выполнить изысканней
```bash
echo "12345" > pass.txt && cat pass.txt | docker login -u user --password-stdin 192.168.0.150:8101
```

##### Работа с приватрым репозиторием apt
update upgrade
sudo apt-get install gpg
генирируем ключ
gpg --gen-key
копируем идентификатор
gpg --list-keys
gpg --export-secret-key --armor или gpg --export-secret-key --armor 9E2C94E689CE704FC3F9B10A5A007D8045B4FA18
копируем ключ, вставляем в интерфейс Nexus 
установим distribution `main`
сохраним
Скачаем произвольный deb пакет и загрузим в репозиторий
открываем 
vim /etc/apt/sources.list 
добавляем строку в конец файла
deb http://192.168.0.150:8081/repository/apt/ main main

выполним 
sudo apt update
Может возникнуть следующая ошибка
```
Ошб:1 http://192.168.0.150:8081/repository/apt main InRelease
  Следующие подписи не могут быть проверены, так как недоступен открытый ключ: NO_PUBKEY 6C64403BD691832D
```
Сделаем ключ доступным
gpg --armor --export 6C64403BD691832D | sudo apt-key add -
выполним
sudo apt update

Скачаем еще один произвольный deb пакет
Загрузим из CLI данный deb пакет, предварительно переименовае его в test.deb
curl -u "user:12345" -H "Content-Type: multipart/form-data" --data-binary "@./test.deb" "http://192.168.0.150:8081/repository/apt/"

*Практические задания*

Долнительные материалы
https://www.youtube.com/watch?v=6WjwrZknYVk 20:00
https://tproger.ru/articles/kak-sozdat-apt-repozitorij-v-sonatype-nexus-3/
https://cloud.croc.ru/blog/byt-v-teme/ustanovka-docker-repozitoriya/