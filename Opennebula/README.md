#Установка (DEBIAN)

### Предварительная тодготовка сервера

Перед развертыванием Opennebula необходимо выполнить следующие требования:

    4 GiB RAM.
    20 GiB free space on disk.
    public IP address (FE-PublicIP)
    privileged user access ( root )
    openssh-server package installed.
    operating system: RHEL/AlmaLinux 8 or 9, Debian 10 or 11, Ubuntu 20.04 or 22.0.4.
    open ports: 22 (SSH), 80 (Sunstone), 2616 (FireEdge), 5030 (OneGate).

- Представленные требования нам говорят, что мы должны проводить устновку из под привилигерованного пользователя.

- Должен быть установлен ssh server и 

- дополнительно требуеся установить terraform.

- Необходимо установить KVM, если подразумевается что ваша сервер не будет являться только масте сервером, а будет выступать в качестве ноды для выделенися ресурсов.

- установка репозитория на сервер 
https://docs.opennebula.io/6.8/installation_and_configuration/frontend_installation/opennebula_repository_configuration.html


- установка ноды 
https://docs.opennebula.io/6.6/open_cluster_deployment/kvm_node/kvm_node_installation.html


