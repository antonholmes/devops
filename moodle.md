Установка Moodle при помощи Docker

##### Подключение репозитория
Обновим индекс пакетов:

```bash
sudo apt update
```

Установим зависимости:
```bash
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

Загрузим и установим пакет Docker. Добавим в систему GPG-ключ репозитория Docker:

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
Добавим этот репозиторий Docker в APT:

```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
```

Обновим индекс пакетов:
```bash
    sudo apt update
```
Следующая команда позволит переключиться из репозитория Debian в репозиторий Docker:
```bash
    apt-cache policy docker-ce
```
Команда вернет следующую информацию:

    docker-ce:
    Installed: (none)
    Candidate: 5:18.09.7~3-0~debian-buster
    Version table:
    5:18.09.7~3-0~debian-buster 500
    500 https://download.docker.com/linux/debian buster/stable amd64 Packages

Установим DockerCE, который необходим для быстрой сборки, отладки и
развертывания приложения:
```bash
    sudo apt install docker-ce install docker-ce docker-ce-cli containerd.io
```
После этого Docker будет установлен Также выше выполненная команда
запустит службу и настроит автозапуск процесса. Чтобы убедиться в том,
что служба работает, запросим её состояние:
```bash
    sudo systemctl status docker
```
Команда вернет следующую информацию:

    docker.service - Docker Application Container Engine
    Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
    Active: active (running) since Mon 2019-07-08 15:11:19 UTC; 58s ago
    Docs: https://docs.docker.com
    Main PID: 5709 (dockerd)
    Tasks: 8
    Memory: 31.6M
    CGroup: /system.slice/docker.service
    └─5709 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Теперь в системе работает сервис Docker и есть доступ к утилите
командной строки docker.

### Docker Compose

средство, разработанное для помощи в определении и совместном использовании многоконтейнерных приложений. С помощью средства Compose можно создать файл YAML для определения служб и с помощью одной команды запускать и останавливать все, что необходимо

Docker Compose можно установить из официальных репозиториев Debian, эти версии немного отстают от последней, и поэтому в данном руководстве мы будем устанавливать Docker из репозитория GitHub. Будем используем флаг -o для указания выходного файла вместо переадресации вывода. Такой синтаксис позволяет избежать ошибки «отказ в разрешении» при использовании sudo.

Проверим текущий выпуск и при необходимости обновим его с помощью
следующей команды:
```bash
    sudo curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```
После этого мы настроим разрешения:
```bash
    sudo chmod +x /usr/local/bin/docker-compose
```
Проверим, что установка прошла успешно, с помощью проверки версии:
```bash
    docker-compose --version
```
В результате должна быть выведена установленная нами версия:

    Output
    docker-compose version 1.25.3, build d4d1b42b

2) Запускаем скрипт командой bash ./Docker_install.sh (дожидаемся окончания заугрузки и установки компонентов)

### Настройка Docker


По умолчанию команда docker требует привилегий root (или доступа к команде sudo). Также её можно запускать в группе docker, которая создаётся автоматически во время установки.

Если попытаться запустить команду docker без префикса sudo и вне группы docker, то получим ошибку:

    docker: Cannot connect to the Docker daemon. Is the docker daemon running on this host?.
    See 'docker run --help'.

Для того чтобы не пришлось набирать префикс sudo каждый раз, когда нам необходимо запустить команду docker, добавим своего пользователя в группу docker:
```bash
    sudo usermod -aG docker $USER
```
Чтобы активировать это изменение, выйдем из системы и войдём снова, или же введём:
```bash
    su - $USER
```
При этом будет запрошен пароль нашего пользователя.

Убедимся, что пользователь добавлен в группу:
```bash
    id -nG $USER
```
Чтобы добавить в группу docker пользователя, который не является текущим, укажем в команде его имя:
```bash
    sudo usermod -aG docker username
```
Docker установлен и готов к работе.

Скачаем docker-compose.yml c настройками по умолчанию для Moodle.
```bash
curl -sSL https://raw.githubusercontent.com/bitnami/bitnami-docker-moodle/master/docker-compose.yml > docker-compose.yml
```
Развернем Moodle с конфигурацием по умолчанию следующей командой
```bash
docker-compose up -d
```
Проверим работоспособность контейнеров следующими командами
```bash
minikube@minikube:~/moodle_test$ docker-compose ps
        Name                    Command           State           Ports         
--------------------------------------------------------------------------------
moodle_test_mariadb_1   /opt/bitnami/scripts/ma   Up      3306/tcp              
                        ria ...                                                 
moodle_test_moodle_1    /opt/bitnami/scripts/mo   Up      0.0.0.0:80->8080/tcp,:
                        odl ...                           ::80->8080/tcp, 0.0.0.
                                                          0:443->8443/tcp,:::443
                                                          ->8443/tcp  
```

Moodle запущен совместно с базой данных MariaDB. Через примерно 10 минут. Сервис будет доступен по адресу localhost:80.
Проверим работоспособность и логи контейнеров.
```
minikube@minikube:~/project/devops$ curl localhost:80/
<!DOCTYPE html>

<html  dir="ltr" lang="en" xml:lang="en">
<head>
    <title>New Site</title>
    <link rel="shortcut icon" href="http://localhost/theme/image.php/boost/theme/1645025670/favicon" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="moodle, New Site" />
<link rel="stylesheet" type="text/css" href="http://localhost/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css" /><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="http://localhost/theme/styles.php/boost/1645025670_1645025827/all" />
<script>
```

Войдем внутрь контейнера с MariaDB и проверим ее работоспособность
```
docker exec -it moodle_test_mariadb_1 bash
mysql -u root  bitnami_moodle
show databases;
```
Для отслеживания логов используйте
`docker container logs имя контейнера`


Все варианты установки Moodle представлена на Docker hub. https://hub.docker.com/r/bitnami/moodle 


Установка Moodle при помощи Kubernetes

Для быстрой установки используем Helm. Helm — это средство упаковки с открытым исходным кодом, которое помогает установить приложения Kubernetes и управлять их жизненным циклом. Аналогично диспетчерам пакетов Linux, таких как APT и Yum, Helm используется для управления чартами Kubernetes, представляющими собой пакеты предварительно настроенных ресурсов Kubernetes
Для этого установим его из исходников
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh


[server@openstack helm]$ helm repo add bitnami https://charts.bitnami.com/bitnami
"bitnami" has been added to your repositories
[server@openstack helm]$ helm install my-moodle bitnami/moodle --version 12.1.5
NAME: my-moodle
LAST DEPLOYED: Tue Feb 22 09:45:55 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: moodle
CHART VERSION: 12.1.5
APP VERSION: 3.11.5** Please be patient while the chart is being deployed **

1. Get the Moodle&trade; URL:

  NOTE: It may take a few minutes for the LoadBalancer IP to be available.
        Watch the status with: 'kubectl get svc --namespace default -w my-moodle'

  export SERVICE_IP=$(kubectl get svc --namespace default my-moodle --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")
  echo "Moodle&trade; URL: http://$SERVICE_IP/"

2. Get your Moodle&trade; login credentials by running:

  echo Username: user
  echo Password: $(kubectl get secret --namespace default my-moodle -o jsonpath="{.data.moodle-password}" | base64 --decode)

  
