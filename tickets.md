Теория
1. Что такое Docker?
2. Какие преимущества предоставляет Docker?
3. Чем Docker отличается от виртуализации на уровне операционной системы (OS-level virtualization)?
4. Какие основные компоненты входят в архитектуру Docker?
5. Что такое контейнер в Docker?
6. Какие существуют способы создания контейнера в Docker?
7. Какие различия между образом (image) и контейнером (container)?
8. Какие команды используются для работы с Docker контейнерами?
9. Как можно передавать данные между контейнерами в Docker?
10. Что такое Dockerfile и для чего он используется?
11. Какие инструкции можно использовать в Dockerfile?
12. Какие команды используются для сборки и запуска контейнера из Dockerfile?
13. Что такое Docker Compose и для чего он используется?
14. Какие основные компоненты входят в архитектуру Docker Compose?
15. Какие файлы используются для определения сервисов в Docker Compose?
16. Какие команды используются для работы с Docker Compose?
17. Что такое Docker Swarm и для чего он используется?
18. Какие основные компоненты входят в архитектуру Docker Swarm?
19. Какие команды используются для работы с Docker Swarm?
20. Что такое Docker Registry и для чего он используется?
21. Какие основные публичные Docker Registry существуют?
22. Как можно создать и использовать собственный приватный Docker Registry?
23. Какие механизмы Docker используются для изоляции контейнеров?
24. Что такое Docker Volume и для чего он используется?
25. Какие виды сетей можно создать в Docker?
26. Как можно связывать контейнеры в Docker?
27. Что такое Docker Overlay Network и для чего он используется?
28. Как можно масштабировать приложение с помощью Docker?
29. Как можно управлять ресурсами контейнера в Docker?
30. Что такое Docker Healthcheck и для чего он используется?
31. Какие механизмы доступны для мониторинга и логирования контейнеров в Docker?
32. Какие механизмы используются для обновления контейнеров в Docker?
33. Какие механизмы используются для обеспечения безопасности контейнеров в Docker?
34. Как можно настроить доступ к контейнеру извне в Docker?
35. Что такое Docker Machine и для чего он используется?
36. Что такое Docker Toolbox и для чего он используется?
37. Как можно использовать Docker в разработке микросервисных приложений?
38. Как можно использовать Docker для тестирования приложений?
39. Как можно использовать Docker для развертывания приложений в продакшн среде?
40. Какие практики и инструменты существуют для управления и автоматизации Docker инфраструктуры?
41. Как создать новый Pod в Kubernetes?
42. Как удалить Pod в Kubernetes?
43. Как создать Deployment в Kubernetes?
44. Как просмотреть список всех Pods в текущем Namespace?
45. Как проверить статус Pod в Kubernetes?
46. Как изменить конфигурацию Deployment?
47. Как создать Service для Pod?
48. Как получить информацию о ресурсах кластера?
49. Как выполнить команду внутри работающего Pod?
50. Как просмотреть логи конкретного Pod?

Практика
1. Создайте Docker контейнер с базовым образом Ubuntu.
2. Установите в контейнере веб-сервер Nginx.
3. Запустите контейнер и проверьте, что веб-сервер доступен по адресу localhost.
4. Создайте Docker образ, включающий ваше собственное веб-приложение.
5. Загрузите образ на Docker Hub.
6. Запустите два контейнера с использованием одного и того же образа, но с разными переменными окружения.
7. Создайте Docker сеть и подключите к ней несколько контейнеров.
8. Настройте контейнеры для автоматического перезапуска при сбое.
9. Создайте Docker образ, включающий базу данных MySQL и веб-приложение, использующее эту базу данных.
10. Подключите внешний том к контейнеру, чтобы сохранить данные базы данных.
11. Создайте Docker образ, включающий Python приложение, и запустите его контейнер.
12. Определите переменные окружения в Dockerfile и передайте их в контейнер при запуске.
13. Установите Docker Compose и используйте его для запуска нескольких контейнеров одновременно.
14. Создайте Docker образ, включающий Node.js приложение, и запустите его контейнер.
15. Настройте Docker контейнер для масштабирования с помощью инструментов оркестрации, таких как Docker Swarm или Kubernetes.
16. Используйте Docker для развертывания приложения Django.
17. Создайте собственный Docker образ, основанный на базовом образе Alpine Linux.
18. Разверните базу данных MongoDB в Docker контейнере и подключитесь к ней с помощью приложения на Node.js.
19. Создайте Docker образ, включающий приложение на Java, и запустите его контейнер.
20. Оптимизируйте Dockerfile и сократите размер и время сборки образа.
21. Напишите манифест YAML для создания Pod, который запускает контейнер с образом Nginx. Запустите Pod и проверьте его статус.
22. Создайте ReplicaSet для запуска трех экземпляров Pod с Nginx. Убедитесь, что ReplicaSet корректно масштабирует количество Pod.
23. Создайте Service типа ClusterIP для доступа к Pod с Nginx. Проверьте, что можно получить доступ к Nginx через Service.
24. Создайте Pod, который использует переменные окружения для задания параметров конфигурации. Проверьте, что переменные доступны в контейнере.
25. Напишите манифест YAML для создания Deployment, который использует образ nginx:latest. Убедитесь, что Deployment успешно развернут и работает.
26. Создайте ConfigMap для хранения конфигурационных данных Nginx. Подключите ConfigMap к Pod и используйте его в приложении.
27. Настройте Persistent Volume (PV) и Persistent Volume Claim (PVC) для хранения данных. Создайте Pod, который использует PVC для доступа к хранилищу.
28. Установите инструмент мониторинга (например, Prometheus) в кластер Kubernetes. Настройте сбор метрик и проверьте, что данные отображаются в интерфейсе.
29. Используйте kubectl для получения информации о состоянии кластера, таких как Nodes, Pods, Services и Deployments. Напишите скрипт, который выводит информацию о всех ресурсах в кластере.
30. Напишите манифест YAML для создания StatefulSet, который развернет три экземпляра приложения с базой данных (например, MySQL). Убедитесь, что каждый Pod имеет свой уникальный идентификатор.
31. Создайте Secret для хранения пароля вашей базы данных. Настройте Pod, чтобы использовать этот Secret в своих переменных окружения.
32. Установите и настройте Ingress-контроллер (например, NGINX) и создайте ресурс Ingress для управления внешним доступом к вашему приложению.